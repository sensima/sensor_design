# -*- coding: utf-8 -*-
"""Misc helpers submodule (misc)
=============================

Collection of misc helper functions and classes 

"""

import gzip
import cPickle as pickle
import json
import numpy as np
import os

import logging
logger = logging.getLogger(__name__)

default_coil_geom_folder = os.path.join(os.path.dirname(os.path.realpath(__file__)),'coils_geom')

#======================================================================
# Functions (object persistence)

#----------------------------------------------------------------------
def save_object(s, filename):
    """Helper function to save a Python object to a file

    The file contains a gzipped pickled version of the object.

    """
    with gzip.open(filename, 'wb') as fh:
        pickle.dump(s, fh, 2)

#----------------------------------------------------------------------
def load_object(filename):
    """Helper function to load a Python object from a file

    Assumes that the file has been created with :func:`save_object`,
    i.e. is a gzipped pickled version of the object.

    """
    with gzip.open(filename, 'rb') as fh:
        s = pickle.load(fh)
    return s

#----------------------------------------------------------------------
def save_coil(c, filename=None, suffix='',folder=default_coil_geom_folder):
    """Helper function to save a coil to a JSON file

    :param c: coil structure (a dict but in fact any object which can be represented as a JSON file can be given)
    :param filename: If None (default) the name is constructed from the sensor field in the coil structure
    :param suffix: (has effect only if filename is None) suffix to add to the basename constructed from the sensor field (e.g. '_out'). Use case: avoid overwriting the input file. Default is not to have any suffix.
    :param folder: By default, the repo for coil geometries in the source code. If None, it is simply ignored
    
    """
    if filename is None: filename = c['sensor'] + suffix + '.json'
    f = filename if folder is None else os.path.join(folder,filename)
    with open(f,'wb') as fh: json.dump(c,fh,indent=4)

#----------------------------------------------------------------------
def load_coil(filename, folder=default_coil_geom_folder):
    """Helper function to load a coil from a JSON file

    :param c: coil structure (in fact any object which can be represented as a JSON file)
    :param filename: name of JSON file to load
    :param folder: By default, the repo for coil geometries in the source code. If None, it is simply ignored

    :return: coil structure (dict)
    
    """
    f = filename if folder is None else os.path.join(folder,filename)
    with open(f,'rb') as fh: c = json.load(fh)
    return c
