# -*- coding: utf-8 -*-
"""Script to compute pancake coil

********************************
GIVEN UP FOR NOW: 
too many things to untangle, including the additional file business

Left in a state which should not run (on purpose)
********************************

The idea is to put all the intermediary and computed results in the coil
dict and to output this to a JSON file (potentially the same as the input
one since input fields are left untouched)

From the "if __name__ == "__main__":" section of pyfile:`pancake_coils_calc.py`

__author__      = "Gilles Santi"

2020-08-03

"""
from logging.handlers import RotatingFileHandler
import logging

import numpy as np
import scipy.constants as cnst
from scipy import signal

import matplotlib.pyplot as plt

import json
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
import sys

from pancake_coils_calc import calculate_self_capacitance, calculate_self_inductance, calculate_resistance, calculate_bode_diagrams
import pancake_coils_draw


# logger setup
logger = logging.getLogger()
hdlr = RotatingFileHandler(
    "./_logs/coils_calc.log", mode="a", maxBytes=5 * 1024 * 1024, backupCount=2, encoding=None, delay=0
)
logFormatter = logging.Formatter("%(asctime)s :: %(levelname)s :: %(message)s")
hdlr.setFormatter(logFormatter)
logger.addHandler(hdlr)
# set logger level
logger.setLevel(logging.DEBUG)
logger.setLevel(logging.INFO)

logToConsole = True
# additional setup, to log to console too - more for debug or terminal operations
if logToConsole:
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    logger.addHandler(consoleHandler)


filename = dir_path + "/" + sys.argv[1] #'VB4_square.json'

with open(filename) as json_file:
    coil = json.load(json_file)

def mylog(msg,fulltxt):
    logger.info(msg)
    fulltxt += msg + '\n'

out_filename = os.path.splitext(filename)[0] + '_out.json'
    
infotxt = ''
    
mylog("################### ---- SENSOR: %s ---- ###################" % coil['sensor'],infotxt)
mylog('---- SENSOR: '+coil['sensor']+' ----',infotxt)
mylog("x dimension: {:.2f} mm".format(coil['a']*1.0e3),infotxt)
mylog("y dimension : {:.2f} mm".format(coil['b']*1.0e3),infotxt)
mylog("number of layers : {:d}".format(coil['layers']),infotxt)
mylog("number of turns : {:d}".format(coil['n']),infotxt)
mylog("track width : {:.2f} um".format(coil['cu_w']*1e6),infotxt)
mylog("track clearance : {:.2f} um".format(coil['cu_d']*1e6),infotxt)

    logger.info("---- SELF CAPACITANCE ----")
    C = calculate_self_capacitance(coil)
    logger.info("---- SELF INDUCTANCE ----")

    L = calculate_self_inductance(coil)
    logger.info("---- RESISTANCE ----")

    R = calculate_resistance(coil)
    

    logger.info("---- INFO ----")

    fn = (1/np.sqrt(C*L))/(2*np.pi)
    logger.info("Estimated self-resonance: {:.2f} kHz".format(fn/1e3))

    Q = 1/R*np.sqrt(L/C)
    logger.info("Estimated quality factor: {:.2f}".format(Q))

    xsi = 0.5*R*np.sqrt(C/L)
    logger.info("Estimated damping factor: {:.2f}".format(xsi))

    wl = (R/L)/(2*np.pi)
    logger.info("Estimated electric pole: {:.2f} kHz".format(wl/1.e3))
 
    logger.info("---- DEVIATION TO REAL MEASUREMENTS ----")

    exp_L = coil['exp_L'] # load some experimental values (if available)
    exp_C = coil['exp_C'] # load some experimental values (if available)
    exp_R = coil['exp_R'] # load some experimental values (if available)
    # exp_F = coil['exp_F']

    if exp_L > 0:
        errL = L - exp_L
        logger.info("Experimental inductance {:.2f} uH -> abs error: {:.2f} uH ({:.2f}%)".format(exp_L*1e6, errL*1e6, errL/exp_L*100))

    if exp_C > 0:
        errC = C - exp_C
        logger.info("Experimental capacitance {:.2f} pF -> abs error: {:.2f} pf ({:.2f}%)".format(exp_C*1e12, errC*1e12, errC/exp_C*100))


    if exp_R > 0:
        errR = R - exp_R
        logger.info("Experimental resistance {:.2f} ohm -> abs error: {:.2f} ohm ({:.2f}%)".format(exp_R, errR, errR/exp_R*100))
        
    if exp_L > 0 and exp_C > 0:
        wexp = 1/np.sqrt(exp_L*exp_C)
        logger.info("Experimental resonance frequency {:.2f} kHz".format(1e-3*wexp/(2*np.pi)))

 

    file.close()

    pancake_coils_draw.draw_coil(coil,name)

    calculate_bode_diagrams(R,L,C,sensor_name=coil['sensor'])

    mylog


    plt.show()
    

