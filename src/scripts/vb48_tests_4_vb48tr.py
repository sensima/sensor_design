# -*- coding: utf-8 -*-
"""Script for creating VB48 coils (2)
==============================

Loading some of the results from Enrico (originally from C:\Users\gille\SSSc-MD\design\coils\SMD_VB48\out_py_scripts)

With ipython:
>>> cd C:\Users\gille\Documents\codes\sensor_design\src
>>> activate base2
>>> ipython --gui=qt --matplotlib=qt

then, cut and paste parts.

or run it with python:
>>> cd C:\Users\gille\Documents\codes\sensor_design\src\scripts
>>> activate base2
>>> python vb48_test_1.py

"""
import os
from os.path import join, dirname
import sys
try:
    script_path = dirname(os.path.realpath(__file__))
    src_path = join(script_path,'..')
except:
    src_path = 'C:/Users/gille/Documents/codes/sensor_design/src'

sys.path.append(src_path)

import json
from pancake_coils_calc import calculate_coil, calc_D
from pancake_coils_draw import draw_coil
from misc import load_coil, save_coil
import matplotlib.pyplot as plt

coils_folder = join(src_path,'coils_geom')

coils = [
    load_coil('VB48T_A_out.json'),
    load_coil('VB48TR_A.json'),
    #load_coil('RS_TX_0004S1_RS_VB48.json'),
]

results = {}
for c in coils:
    results[c['sensor']] = calculate_coil(c)

for n,r in results.iteritems():
    print '{}\n\n'.format(r['calculate_coil.message'])
    #print '    Stot = {:.1f} mm2'.format(r['Stot']*1e6)

#print 'Stot ratio = {:.1f}'.format(results['VB48T_A']['Stot']/results['VB48TR_A']['Stot'])

plt.close('all')
for n,r in results.iteritems():
    draw_coil(r['coil'],figure=plt.figure())


for c in coils[1:]: save_coil(c,suffix='_out')

plt.show()

