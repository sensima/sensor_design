# -*- coding: utf-8 -*-
"""Script for creating VB48 coils (VB45)
==============================

For some variations of VB46R_E leaving a 15 mm gap in the centre.

See also (OneNote page):
https://onedrive.live.com/view.aspx?resid=72B10DD5657DC01C%2111803&id=documents&wd=target%28Simulations.one%7CEBA3E078-03F2-43AE-B1CE-9732ED2B3C07%2FSensor%20design%3A%20VB48R%20%28modification%20of%20VB46R%20coils%20for%20larger%7CBE337A02-2359-4E42-BD42-936E42C62BE8%2F%29
onenote:https://d.docs.live.net/72b10dd5657dc01c/SSSc-MD/SSSc%20Devs%20(internal)/Simulations.one#Sensor%20design%20VB48R%20(modification%20of%20VB46R%20coils%20for%20larger&section-id={EBA3E078-03F2-43AE-B1CE-9732ED2B3C07}&page-id={BE337A02-2359-4E42-BD42-936E42C62BE8}&end

With ipython:
>>> cd C:\Users\gille\Documents\codes\sensor_design\src
>>> activate base2
>>> ipython --gui=qt --matplotlib=qt

then, cut and paste parts.

or run it with python:
>>> cd C:\Users\gille\Documents\codes\sensor_design\src\scripts
>>> activate base2
>>> python vb45_test_1.py

"""
import os
from os.path import join, dirname
import sys
try:
    script_path = dirname(os.path.realpath(__file__))
    src_path = join(script_path,'..')
except:
    src_path = 'C:/Users/gille/Documents/codes/sensor_design/src'

sys.path.append(src_path)

import json
from pancake_coils_calc import calculate_coil, calc_D
from pancake_coils_draw import draw_coil
from misc import load_coil, save_coil
import matplotlib.pyplot as plt

coils_folder = join(src_path,'coils_geom')

#with open(join(coils_folder,'VB46R_E.json'),'rb') as fh: c0 = json.load(fh)
c0 = load_coil('VB45_A_rect.json')
out = calculate_coil(c0)
print out['calculate_coil.message']


coils = [c0]

D = calc_D(37e-3,15e-3,32)

c = c0.copy()
c.update(
    {
        "_notes": "VB45T_A1 from VB45_A_rect: single layer",
        "sensor": "VB45T_A1",
        "cu_d": D - c0['cu_w'],
        "n": 32,
        "layers": 1,
    }
)
coils.append(c)
print calculate_coil(c)['calculate_coil.message']

import numpy as np
def calc_Z(f,L,R,C=None):
    om = 2*np.pi*f
    if C is None:
        Z = R + 1j*om*L
    else:
        Z = R + 1. / (1./(1j*om*L) + 1j*om*C)
    return Z

c = c0
        
f = np.arange(0,2e6,1e3)
Z1 = calc_Z(f,c['calculated']['L'],c['calculated']['R'])
Z2 = calc_Z(f,c['calculated']['L'],c['calculated']['R'],c['calculated']['C'])

plt.figure()
plt.plot(f/1000,Z1.real,'b--')
plt.plot(f/1000,Z1.imag,'b-.')
plt.plot(f/1000,np.abs(Z1),'b-')
plt.plot(f/1000,Z2.real,'r--')
plt.plot(f/1000,Z2.imag,'r-.')
plt.plot(f/1000,np.abs(Z2),'r-')
plt.grid()








D = calc_D(115e-3,16e-3,40)
print 'D={:.1f}'.format(D)
c = c0.copy()
c.update(
    {
        "_notes": "VB48 from VB46R_D: same outer size",
        "sensor": "VB48R_D1",
        "cu_d": 1053e-6,
        "a": 115e-3,
        "n": 40,
        "layers": 1,
    }
)
coils.append(c)
print calculate_coil(c)['calculate_coil.message']

D = calc_D(115e-3,16e-3,50)
print 'D={:.1f}'.format(D*1e6)
c = c1.copy()
c.update(
    {
        "_notes": "VB48 from VB46R_E: smaller outer size and single layer",
        "sensor": "VB48R_E4",
        "cu_d": 800e-6,
        "a": 115e-3,
        "n": 50,
        "layers": 1,
    }
)
coils.append(c)
print calculate_coil(c)['calculate_coil.message']

D = calc_D(115e-3,16e-3,40)
print 'D={:.1f}'.format(D*1e6)
c = c2.copy()
c.update(
    {
        "_notes": "VB48 from VB46R_F: smaller outer size",
        "sensor": "VB48R_F1",
        "cu_d": 1053e-6,
        "a": 115e-3,
        "n": 40,
        "layers": 2,
    }
)
coils.append(c)
print calculate_coil(c)['calculate_coil.message']

for c in coils[1:]: save_coil(c)

results = {}
for c in coils:
    results[c['sensor']] = calculate_coil(c)

for n,r in results.iteritems():
    print '{}\n\n'.format(r['calculate_coil.message'])

plt.close('all')
for n,r in results.iteritems():
    draw_coil(r['coil'],figure=plt.figure())

for c in coils[1:]: save_coil(c,suffix='_out')

plt.show()

# c0 is now loaded from JSON file

### c0 = {
###     "_notes": "pancake coil used to build up the integrated concept VB46 - coil 2 (middle)",
###     "sensor": "VB46R_E",
###     "cu_w": 200e-6, 
###     "cu_d": 3500e-6,
###     "cu_t": 35e-6,
###     "layers": 2,
###     "layers_d": 1.2e-3,
###     "layers_skewing":False,
###     "line_w" : 250e-6,
###     "line_d" : 250e-6,
###     "line_length" : 5e-2,
###     "epsilon_r" : 4.7,
###     "geometry" : "rectangular",
###     "n" : 16,
###     "a" : 120e-3,
###     "b" : 380e-3,
###     "r" : 0,
###     "exp_L":240e-6,
###     "exp_R":0,
###     "exp_C":0,
### }

