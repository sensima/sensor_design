# -*- coding: utf-8 -*-
"""
This module contains some useful functions for the calculation of the magnetic field
induced by a current line. the model is based on the well-know Biot-Savart formula.

I just decided to convert some of my old scripts (dated back to my PhD studies...), 
as it turn out can be useful 

__author__      = "Enrico Gasparin"

2020-05-31 (scripts original taken from some old 2015 matlab scripts....)
"""

from logging.handlers import RotatingFileHandler
import logging
# setup rotating file logger
logger = logging.getLogger()
hdlr = RotatingFileHandler(
    "./_logs/coils_calc.log", mode="a", maxBytes=5 * 1024 * 1024, backupCount=2, encoding=None, delay=0
)
logFormatter = logging.Formatter("%(asctime)s :: %(levelname)s :: %(message)s")
hdlr.setFormatter(logFormatter)
logger.addHandler(hdlr)
# set logger level
logger.setLevel(logging.DEBUG)
logger.setLevel(logging.INFO)

logToConsole = True
# additional setup, to log to console too - more for debug or terminal operations
if logToConsole:
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    logger.addHandler(consoleHandler)

import numpy as np
import scipy.constants as cnst
import matplotlib.pyplot as plt

import json
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
import sys

import numpy as np
import discretize_coil

def calc_dB(i, dl, r):      
    '''
    return the partial integral of the dB

    i: the current value (can be complex)
    dl: numpy array of the increments in the 3 xyz coordinates
    r: numpy array of the distance to the targeted point for the 3 coordinates
    '''       
    
    mu0 = 4*np.pi*1e-7
    
    
    dB = mu0/(4*np.pi)*i*np.cross(dl,r)/(np.linalg.norm(r)**3)
    
    return dB


def calc_dA(i, dl, r):      
    '''
    return the partial magentic vector potential (to verify)

    i: the current value (can be complex)
    dl: numpy array of the increments in the 3 xyz coordinates
    r: numpy array of the distance to the targeted point for the 3 coordinates
    '''       
    
    mu0 = 4*np.pi*1e-7
    
    
    dA = mu0/(4*np.pi)*i/r
    
    return dA


def calc_B_over_coordinates(u_xyz, p_xyz, i_val):
    '''

    u: vector numpy shape(3,N) of the coordinates of the current path "sampling" point
    p: coordinates of the target point
    i: the current (can be complex)
    '''

    # transpose the vector for ease of manipulation
    u_xyz = u_xyz.T
    n = u_xyz.shape[0] # number of samples

    B = 0   
    dl = np.zeros((3))
    for j in range(n-1):

        dl = u_xyz[j+1] - u_xyz[j]
        r =  p_xyz - 0.5*(u_xyz[j] + u_xyz[j+1] ) 
        dB = calc_dB(i_val, dl, r)
        B += dB

    return B

def calc_phi_over_coordinates(u_xyz, p_xyz, i_val):
    '''
    return the flux linkage

    u_xyz: coordinates of the target point
    B_xyz: vector of the magnetic field
    '''

    phi = 0

    return phi


def test():
    '''
    calculate the magnetic field on one point given a circle of current
    '''   
    R = 1.0
    p = [0,0,1]

    n = 100
    theta = np.linspace(0, 2*np.pi, n)
    x = R*np.cos(theta)
    y = R*np.cos(theta)

    u_xyz = np.zeros((3,n))

    u_xyz[0] = R*np.cos(theta)
    u_xyz[1] = R*np.sin(theta)

    return calc_B_over_coordinates(u_xyz, [0,0,1], 1.0 + 1j)



if __name__ == "__main__":

    # print(test())

    filename = dir_path + "/" + sys.argv[1] #'VB4_square.json'

    with open(filename) as json_file:
        coil = json.load(json_file)

    u = discretize_coil.discretize(coil)
    print(u.T[-1])

    



    # define rectangles over the coil (for rectangular coil)
    nx = 5
    ny = 5
    a0 = coil['a']
    b0 = coil['b']
    dphi = 0
    phi = 0

    for nl in range(coil['layers']):
        for nw in range(coil['n']):
            a = a0 - nw*(coil['cu_w'] + coil['cu_d'])
            b = b0 - nw*(coil['cu_w'] + coil['cu_d'])
            dx = b/nx
            dy = a/ny
            print(dx*dy)

            xvect = np.array([])
            yvect = np.array([])
            zvect = np.array([])

            x = 0
            y = 0
            z = 1e-6 - nl*coil['layers_d'] #height


            plt.figure(1)

            for j in range(nx):
                
                y = 0
                print(nl, nw, j)
                for k in range(ny):

                    # xvect = np.concatenate([xvect,xvect[-1] + dx])
                    # yvect = np.concatenate([yvect,yvect[-1] + dy])
                    # zvect = np.concatenate([zvect,z])

                    
                    


                    B = calc_B_over_coordinates(u, [x,y,z], 1.0)
                    dphi = (B*(dx*dy))
                    phi += dphi

                    y +=dy
            
            
                    plt.plot(x,y,'*')
                    # print(j,k,x,y, B)
                    

                    # 
                x += dx


    # plt.axes('equal')
    
    print phi

    plt.show()