"""
    functions used to discretize a coil in a vecotr of coordinates

    __author__      = "Enrico Gasparin"

    2020-03-04


"""

import json
import matplotlib.pyplot as plt

import os, sys
dir_path = os.path.dirname(os.path.realpath(__file__))


import numpy as np



def discretize(coil, nsamples = 100, res = 1e-3):

    # coil construction
    cu_w = coil['cu_w']   #copper width
    cu_d = coil['cu_d']   #copper distance
    cu_t = coil['cu_t']    #copper thicknes
    layers = coil['layers']
    layers_d = coil['layers_d'] # distance among layers

    # coil interconnection
    line_w = coil['line_w']
    line_d = coil['line_d']
    line_length = coil['line_length']

    # electric properties
    epsilon_r = coil['epsilon_r'] # good approximation for FR4

    # geometry
    n = coil['n']  # turns
    geometry = coil['geometry']
    a = coil['a']
    b = coil['b']
    r = coil['r'] #outer radius -- for spiral only


    fig = plt.figure(1)


    if geometry == "rectangular":
        x0 = 0
        y0 = 0
        z0 = 0

        xvect = np.array([])
        yvect = np.array([])
        zvect = np.array([])

        dw = cu_d + cu_w
        for nl in range(layers):
            for j in range(n):
                
                # first line
                x0 = 0 + j*dw 
                y0 = a - j*dw 
                z0 = -nl*layers_d


                x1 = b - j*dw 
                y1 = a - j*dw 
                z1 = -nl*layers_d

                # int((x1-x0)/res)
                x = np.linspace(x0,x1,nsamples)
                y = np.linspace(y0,y1,nsamples)
                z = np.linspace(z0,z1,nsamples)
                xvect = np.concatenate([xvect,x])
                yvect = np.concatenate([yvect,y])
                zvect = np.concatenate([zvect,z])


                # second line
                x0 = b - j*dw 
                y0 = a - j*dw 
                z0 = -nl*layers_d


                x1 = b - j*dw  
                y1 = (1+j)*dw
                z1 = -nl*layers_d

                x = np.linspace(x0,x1,nsamples)
                y = np.linspace(y0,y1,nsamples)
                z = np.linspace(z0,z1,nsamples)
                xvect = np.concatenate([xvect,x])
                yvect = np.concatenate([yvect,y])
                zvect = np.concatenate([zvect,z])

                # third line
                x0 = b - j*dw  
                y0 = (1+j)*dw
                z0 = -nl*layers_d


                x1 = (1+j)*dw  
                y1 = (1+j)*dw
                z1 = -nl*layers_d

                x = np.linspace(x0,x1,nsamples)
                y = np.linspace(y0,y1,nsamples)
                z = np.linspace(z0,z1,nsamples)
                xvect = np.concatenate([xvect,x])
                yvect = np.concatenate([yvect,y])
                zvect = np.concatenate([zvect,z])

                # fourth line
                x0 = (1+j)*dw 
                y0 = (1+j)*dw
                z0 = -nl*layers_d


                x1 = (1+j)*dw  
                y1 = a - (1+j)*dw
                z1 = -nl*layers_d

                x = np.linspace(x0,x1,nsamples)
                y = np.linspace(y0,y1,nsamples)
                z = np.linspace(z0,z1,nsamples)
                xvect = np.concatenate([xvect,x])
                yvect = np.concatenate([yvect,y])
                zvect = np.concatenate([zvect,z])


    plt.figure(1)
    plt.plot(xvect,yvect,'-')
    plt.axis('equal')
    # plt.show()
    print(len(xvect))
    u = np.zeros((3,len(xvect)))
    u[0] = xvect
    u[1] = yvect
    u[2] = zvect
    return u 




if __name__ == "__main__":

    pass


    

    



    

