import os
from re import T
import sys
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
try:
    script_path = os.path.dirname(os.path.realpath(__file__))
    src_path = os.path.join(script_path, '..', '..')
except:
    src_path = 'C:/Users/gille/Documents/codes/sensor_design/src'

print(src_path)
sys.path.append(src_path)

from shielding.shielding import *

################################################################################
################################ No electrodes #################################
################################################################################
D = 1

shield_simple_vert = Shielding(D=D)


shield_simple_vert.add_fill(-14, -303, 14, 0, end_points='end')
shield_simple_vert.reverse_order()
shield_simple_vert.add_fill(14 + 40 - 12/2, 303 + 145, inverse_start_direction=False, end_points='end')
for i in range(6):
    shield_simple_vert.add_vertex(12, 0, relative=True)
    shield_simple_vert.add_fill(40 - 12, 303 + 145, None, -303, end_points='end')
shield_simple_vert.add_vertex(12, 0, relative=True)
shield_simple_vert.add_fill(44, 303 + 145, None, -303, end='bot')
shield_simple_vert.add_fill(10, 420 + 13, None, -303, end_points='end')
shield_simple_vert.translate(-170 - 28, 303 - 210)
shield_simple_vert.remove_duplicated_vertices()
patches = []
for i in range(7):
    for j in range(9):
        patches.append(Rectangle((116 - 246 + i*40 - 12/2., 170 - j*40 - 12/2.), 12, 12, fill=False, ec='b'))
shield_simple_vert.plot_patches.extend(patches)
shield_simple_vert.plot()

shield_simple_hor = Shielding(D=D, vertical=False)
shield_simple_hor.add_fill(-344, 15, 14 + 344, 145 - 15, end_points='end')
shield_simple_hor.reverse_order()
shield_simple_hor.add_fill(344 + 10, -(13 + 40 - 12/2), 14, 145 - 15, inverse_start_direction=False, end_points='end')
for i in range(2):
    shield_simple_hor.add_vertex(0, -12, relative=True)
    shield_simple_hor.add_fill(344 + 10, -(40 - 12), 14, None, end_points='end')
for i in range(6):
    shield_simple_hor.add_vertex(0, -12, relative=True)
    shield_simple_hor.add_fill(14 + 344 + 10, -(40 - 12), 0, None, end_points='end')
shield_simple_hor.add_vertex(0, -12, relative=True)
shield_simple_hor.add_fill(14 + 344 + 10, -54, 0, None, end_points='end')
shield_simple_hor.translate(-170 - 28, 303 - 210)
shield_simple_hor.remove_duplicated_vertices()
patches = []
for i in range(7):
    for j in range(9):
        patches.append(Rectangle((116 - 246 + i*40 - 12/2., 170 - j*40 - 12/2.), 12, 12, fill=False, ec='b'))
shield_simple_hor.plot_patches.extend(patches)
shield_simple_hor.plot()

shield_simple_vert.generate_dxf(save_filename=r'C:\Users\beryl\Documents\git_repos\Sensima\sensor_design\src\shielding\scripts\md7x9y_shield_simple_vert.dxf')
shield_simple_hor.generate_dxf(save_filename=r'C:\Users\beryl\Documents\git_repos\Sensima\sensor_design\src\shielding\scripts\md7x9y_shield_simple_hor.dxf')
plt.show()
