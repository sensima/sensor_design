import os
from re import T
import sys
import matplotlib.pyplot as plt
try:
    script_path = os.path.dirname(os.path.realpath(__file__))
    src_path = os.path.join(script_path, '..', '..')
except:
    src_path = 'C:/Users/gille/Documents/codes/sensor_design/src'

print(src_path)
sys.path.append(src_path)

from shielding.shielding import *

################################################################################
################################ No electrodes #################################
################################################################################
D = 1

shield_simple_vert = Shielding(D=D)
shield_simple_vert.add_fill(-14, -303, 14, 0, end_points='end')
shield_simple_vert.reverse_order()
s = generate_shielding_with_hole_array(
    dimensions=(340 - 10 + 14, 420 + 13 + 15),
    hole_center=(40 + 14, 40 + 13 + 15),
    hole_distance=(40, 40),
    hole_dimensions=(12, 12),
    hole_grid_size=(7, 9),
    D=D,
    start_top=False,
    end='bot'
)
s.translate(14, 145)
shield_simple_vert += s
shield_simple_vert.add_fill(10, 420 + 13, end_points='end')
shield_simple_vert.translate(-170 - 28, 303 - 210)
shield_simple_vert.remove_duplicated_vertices()
shield_simple_vert.plot()

shield_simple_hor = Shielding(D=D)
shield_simple_hor.add_fill(-14 + D, 303, 14 - D, -303, end_points='end', vertical=False)
shield_simple_hor.reverse_order()
s = generate_shielding_with_hole_array(
    dimensions=(340 + 14, 420 + 13),
    hole_center=(40  + 14, 40 + 13),
    hole_distance=(40, 40),
    hole_dimensions=(12, 12),
    hole_grid_size=(7, 9),
    D=D,
    start_top=True,
    end='top',
    vertical=False
)
s.translate(14, 145 - 15)
s.reverse_order()
shield_simple_hor += s
shield_simple_hor.add_fill(344, 15, end_points='end', inverse_start_direction=True, vertical=False)
shield_simple_hor.translate(-170 - 28, 303 - 210)
shield_simple_hor.remove_duplicated_vertices()
shield_simple_hor.plot()

shield_simple_vert.generate_dxf(save_filename=r'C:\Users\beryl\Documents\git_repos\Sensima\sensor_design\src\shielding\scripts\md7x9y_shield_vert.dxf')
shield_simple_hor.generate_dxf(save_filename=r'C:\Users\beryl\Documents\git_repos\Sensima\sensor_design\src\shielding\scripts\md7x9y_shield_hor.dxf')
plt.show()
