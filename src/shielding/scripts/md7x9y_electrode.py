import os
from re import T
import sys
import matplotlib.pyplot as plt
try:
    script_path = os.path.dirname(os.path.realpath(__file__))
    src_path = os.path.join(script_path, '..', '..')
except:
    src_path = 'C:/Users/gille/Documents/codes/sensor_design/src'

print(src_path)
sys.path.append(src_path)

from shielding.shielding import *

################################################################################
############################### With electrodes ################################
################################################################################
D = 1
D_con = 0.5 # Center to center track distance of the electrode wiring
D_int = 1 # Center to center track distance between shielding and electrode wiring

d1_l = 14 + 12 - 2*D_con - D_int - D
d1_s = 14 - 2*D_con - D_int
d2_l = d1_l + D_con
d2_s = d1_s + D_con
d3_l = d2_l + D_con
d3_s = d2_s + D_con

inter_electrode_segment_vert = Shielding(D=D)
inter_electrode_segment_vert = Shielding(D=D)
inter_electrode_segment_vert.add_fill(14 + D_int, d1_l, 0, D, True, end='top')
inter_electrode_segment_vert.add_fill(12, d1_s, None, 12, end='bot')
inter_electrode_segment_vert.add_fill(28, -d1_l, end='top')
inter_electrode_segment_vert.add_fill(12, d1_s, None, 12, end='bot')
inter_electrode_segment_vert.add_fill(28, -d1_l, end='top')
inter_electrode_segment_vert.add_fill(12, d1_s, None, 12, end='bot')
inter_electrode_segment_vert.add_fill(14 + D_con + D_int, -d1_l)
inter_electrode_segment_vert.add_fill(14 - D_con - D_int, d2_l, None, D, end='top')
inter_electrode_segment_vert.add_fill(12, d2_s, None, 12, end='bot')
inter_electrode_segment_vert.add_fill(28, -d2_l, end='top')
inter_electrode_segment_vert.add_fill(12, d2_s, None, 12, end='bot')
inter_electrode_segment_vert.add_fill(28, -d2_l, end='top')
inter_electrode_segment_vert.add_fill(12, d2_s, None, 12, end='bot')
inter_electrode_segment_vert.add_fill(14 + D_int, -d2_l)
inter_electrode_segment_vert.add_fill(14 - D_int, d3_l, None, D, end='top')
inter_electrode_segment_vert.add_fill(12, d3_s, None, 12, end='bot')
inter_electrode_segment_vert.add_fill(54, -d3_l, end='bot')

def add_T_vertical_bar(s):
    s.add_fill(-(14 - D_int), -(94 + D_con - D_int), end='bot')
    s.add_loop_fill(-12, -28)
    s.add_loop_fill(-12, -28, None, -40)
    s.add_fill(-12, -(14 + D_con - D_int), None, -80, end='top')
    s.add_fill(-(14 - D_int), 94 + D_con - D_int, end='top')

inter_electrode_segment_vert.add_fill(-(80 + D_int), -d3_s, None, 0, end='bot')
add_T_vertical_bar(inter_electrode_segment_vert)
inter_electrode_segment_vert.add_fill(-(80 + 2*D_int), -d3_s, None, 0, end='bot')
add_T_vertical_bar(inter_electrode_segment_vert)
inter_electrode_segment_vert.add_fill(-(80 + 2*D_int), -d3_s, None, 0, end='bot')
inter_electrode_segment_vert.translate(-21, -6)

inter_electrode_segment_vert.plot()

inter_electrode_segment_hor = Shielding(D=D, vertical=False)
inter_electrode_segment_hor.add_loop_fill(120 + D_con + D_int - D, d1_s, 0, 0, allow_last=True)
inter_electrode_segment_hor.add_loop_fill(120 - D_con - D, d2_s, 120 + D_con + D_int, None, allow_last=True)
inter_electrode_segment_hor.add_loop_fill(80 - D_int, d3_s, 240 + D_int, None, allow_last=True)
inter_electrode_segment_hor.add_fill(-54, -12, 320, None, inverse_start_direction=True, end='left')
for i in range(6):
    inter_electrode_segment_hor.add_loop_fill(-28, 12, 254 - i*40, None)
inter_electrode_segment_hor.add_loop_fill(-14, 12, 14, None)
inter_electrode_segment_hor.add_fill(320, -(14 - D_int), 0, None, inverse_start_direction=True,  end='left')

def add_T_horizontal_bar(s, pillar_n):
    x_start = pillar_n*40
    s.add_fill(-(26 - D_int - D), -(14 + D_int), x_start - D_int, None, end='right')
    s.add_fill(14 - D_int, -12, x_start - 14, None, end='left')
    s.add_fill(-(26 - D_int - D), -28, x_start - D_int, None, end='right')
    s.add_fill(14 - D_int, -12, x_start - 14, None, end='left')
    s.add_fill(-(26 - D_int - D), -(14 + D_con - D_int), x_start - D_int, None, end='left')
    s.add_fill(-(14 - D_int), 80 + D_con, x_start - 26, None, end='left')

add_T_horizontal_bar(inter_electrode_segment_hor, 6)
add_T_horizontal_bar(inter_electrode_segment_hor, 3)
inter_electrode_segment_hor.translate(-20, 6)

inter_electrode_segment_hor.plot()

dxf = inter_electrode_segment_vert.generate_dxf()
inter_electrode_segment_hor.generate_dxf(dxf=dxf, layer='bottom', save_filename=r'C:\Users\beryl\Documents\git_repos\Sensima\sensor_design\src\shielding\scripts\md7x9y_shielding_E.dxf')

plt.show()

################################################################################
################################## Electrodes ##################################
################################################################################
# D = 1

# electrode_vert = generate_shielding_with_hole_array(
#     dimensions=(80, 80),
#     hole_center=(20, 20),
#     hole_distance=(40, 40),
#     hole_dimensions=(12, 12),
#     hole_grid_size=(2, 2),
#     D=D,
#     start_top=True,
#     end=None,
#     vertical=True
# )
# electrode_vert.translate(-40, 40)
# electrode_vert.plot()

# electrode_hor = generate_shielding_with_hole_array(
#     dimensions=(80, 80),
#     hole_center=(20, 20),
#     hole_distance=(40, 40),
#     hole_dimensions=(12, 12),
#     hole_grid_size=(2, 2),
#     D=D,
#     start_top=True,
#     end=None,
#     vertical=False
# )
# electrode_hor.translate(-40, 40)
# electrode_hor.plot()

# electrode_URcorner_vert = generate_shielding_with_hole_array(
#     dimensions=(70, 80),
#     hole_center=(20, 20),
#     hole_distance=(40, 40),
#     hole_dimensions=(12, 12),
#     hole_grid_size=(2, 2),
#     D=D,
#     start_top=True,
#     end=None,
#     vertical=True
# )
# electrode_URcorner_vert.add_fill(10, 73, end_points='end')
# electrode_URcorner_vert.translate(-40, 40)
# electrode_URcorner_vert.plot()

# electrode_URcorner_hor = Shielding(D=D)
# electrode_URcorner_hor.add_fill(70, 7, 0, -7, inverse_start_direction=True, end_points='end', vertical=False)
# electrode_URcorner_hor.reverse_order()
# s = generate_shielding_with_hole_array(
#     dimensions=(80, 73),
#     hole_center=(20, 13),
#     hole_distance=(40, 40),
#     hole_dimensions=(12, 12),
#     hole_grid_size=(2, 2),
#     D=D,
#     start_top=True,
#     end=None,
#     vertical=False
# )
# s.translate(0, -7)
# electrode_URcorner_hor += s
# electrode_URcorner_hor.translate(-40, 40)
# electrode_URcorner_hor.plot()

# dxf = electrode_vert.generate_dxf()
# electrode_hor.generate_dxf(dxf=dxf, layer='bottom', save_filename=r'C:\Users\beryl\Documents\git_repos\Sensima\sensor_design\src\shielding\scripts\md7x9y_electrode.dxf')
# dxf = electrode_URcorner_vert.generate_dxf()
# electrode_URcorner_hor.generate_dxf(dxf=dxf, layer='bottom', save_filename=r'C:\Users\beryl\Documents\git_repos\Sensima\sensor_design\src\shielding\scripts\md7x9y_electrode_URcorner.dxf')

# plt.show()