from copy import copy
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
# from pancake_coils_draw import generate_dxf

# Shielding outer dimensions
X = 40
Y = 40

# Position of the center of the top-left hole
HCX = 10
HCY = 10

# Hole center to center distance
HDX = 10
HDY = 10

# Hole dimensions
HX = 3
HY = 3

# Hole grid size
HNX = 3
HNY = 3

# Shielding zigzag direction
IS_VERT = True

# Track center to center distance
D = 0.3

##################################
def add_point(e, p):
    e[0].append(p[0])
    e[1].append(p[1])

def translate(e, t):
    for i in range(len(e[0])):
        e[0][i] += t[0]
        e[1][i] += t[1]

def copy_element(e):
    e2 = (copy(e[0]), copy(e[1]))
    return e2

def inverse_element(e):
    e2 = (copy(e[0][::-1]), copy(e[1][::-1]))
    return e2

def add_element(o, e):
    o[0].extend(e[0])
    o[1].extend(e[1])

#################################

big_element = ([], [])
big_element_i = ([], [])
wx = HDX - HX
be_n = int(wx//D) + 1 # number of vertical lines that can fit in the big element
for i in range(be_n - 1):
    if i % 2 == 0:
        add_point(big_element, (i*D, -Y))
        add_point(big_element, ((i + 1)*D, -Y))
        add_point(big_element_i, (i*D, 0))
        add_point(big_element_i, ((i + 1)*D, 0))
    else:
        add_point(big_element, (i*D, 0))
        add_point(big_element, ((i + 1)*D, 0))
        add_point(big_element_i, (i*D, -Y))
        add_point(big_element_i, ((i + 1)*D, -Y))

translate(big_element, (HDX/2. - (be_n - 1)*D/2., 0))
translate(big_element_i, (HDX/2. - (be_n - 1)*D/2., 0))

small_element_mid = ([], [])
small_element_top = ([], [])
small_element_bot = ([], [])
wx = big_element[0][0] + HDX - big_element[0][-1]
wy = HDY - HY
h_mid = (wy - D)/2.
h_top = (HCY - HY/2. - D)/2.
h_bot = (Y - (HCY + (HNY - 1)*HDY + HY/2.) - D)/2.
se_n = int(wx//D) # number of vertical lines that can fit in the small element
if se_n % 2 == 0:
    se_n -= 1
d2 = float(wx)/se_n
for i in range(se_n - 1):
    if i % 2 == 0:
        add_point(small_element_mid, (i*d2, 0))
        add_point(small_element_mid, ((i + 1)*d2, 0))
        add_point(small_element_top, (i*d2, -h_top))
        add_point(small_element_top, ((i + 1)*d2, -h_top))
        add_point(small_element_bot, (i*d2, -Y + h_bot))
        add_point(small_element_bot, ((i + 1)*d2, -Y + h_bot))
    else:
        add_point(small_element_mid, (i*d2, h_mid))
        add_point(small_element_mid, ((i + 1)*d2, h_mid))
        add_point(small_element_top, (i*d2, 0))
        add_point(small_element_top, ((i + 1)*d2, 0))
        add_point(small_element_bot, (i*d2, -Y))
        add_point(small_element_bot, ((i + 1)*d2, -Y))
for i in range(se_n - 1)[::-1]:
    if i % 2 == 0:
        add_point(small_element_mid, ((i + 1)*d2, -D))
        add_point(small_element_mid, (i*d2, -D))
        add_point(small_element_top, ((i + 1)*d2, -h_top - D))
        add_point(small_element_top, (i*d2, -h_top - D))
        add_point(small_element_bot, ((i + 1)*d2, -Y + h_bot + D))
        add_point(small_element_bot, (i*d2, -Y + h_bot + D))
    else:
        add_point(small_element_mid, ((i + 1)*d2, -D - h_mid))
        add_point(small_element_mid, (i*d2, -D - h_mid))
        add_point(small_element_top, ((i + 1)*d2, -2*h_top - D))
        add_point(small_element_top, (i*d2, -2*h_top - D))
        add_point(small_element_bot, ((i + 1)*d2, -Y + 2*h_bot + D))
        add_point(small_element_bot, (i*d2, -Y + 2*h_bot + D))

translate(small_element_mid, (big_element[0][-1] - HDX, (D - HDY)/2.))
translate(small_element_top, (big_element[0][-1] - HDX, 0))
translate(small_element_bot, (big_element[0][-1] - HDX, 0))

small_element_mid_i = inverse_element(small_element_mid)

transition_top = ([], [])
transition_bot = ([], [])
h_top = HCY - HY/2.
h_bot = Y - (HCY + (HNY - 1)*HDY + HY/2.)
for i in range(se_n):
    if i % 2 == 0:
        add_point(transition_top, (i*d2, 0))
        add_point(transition_top, ((i + 1)*d2, 0))
        add_point(transition_bot, (i*d2, -Y))
        add_point(transition_bot, ((i + 1)*d2, -Y))
    else:
        add_point(transition_top, (i*d2, -h_top))
        add_point(transition_top, ((i + 1)*d2, -h_top))
        add_point(transition_bot, (i*d2, -Y + h_bot))
        add_point(transition_bot, ((i + 1)*d2, -Y + h_bot))

translate(transition_top, (big_element[0][-1] - HDX, 0))
translate(transition_bot, (big_element[0][-1] - HDX, 0))

##########################
p = ([0], [0])
start_w = HCX + transition_top[0][0]
start_n = int(start_w//D + 1)
start_d = float(start_w)/(start_n - 1)
for i in range(start_n - 1):
    if i % 2 == 0:
        add_point(p, (i*start_d, -Y))
        add_point(p, ((i + 1)*start_d, -Y))
    else:
        add_point(p, (i*start_d, 0))
        add_point(p, ((i + 1)*start_d, 0))

top_to_bot = start_n % 2 == 1

def add_vertical_segment_around_holes(dx, top_to_bot):
    if top_to_bot:
        e = copy_element(small_element_top)
    else:
        e = copy_element(small_element_bot)
    translate(e, (dx, 0))
    add_element(p, e)

    for j in range(HNY - 1):
        if top_to_bot:
            e = copy_element(small_element_mid)
            translate(e, (dx, -HCY - j*HDY))
        else:
            e = copy_element(small_element_mid_i)
            translate(e, (dx, -HCY - (HNY - 2 - j)*HDY))
        add_element(p, e)

    if top_to_bot:
        e = copy_element(transition_bot)
    else:
        e = copy_element(transition_top)
    translate(e, (dx, 0))
    add_element(p, e)

for i in range(HNX - 1):
    add_vertical_segment_around_holes(HCX + i*HDX, top_to_bot)
    if top_to_bot:
        e = copy_element(big_element_i)
    else:
        e = copy_element(big_element)
    translate(e, (HCX + i*HDX, 0))
    add_element(p, e)
    if be_n % 2 == 1:
        top_to_bot = not top_to_bot

add_vertical_segment_around_holes(HCX + (HNX - 1)*HDX, top_to_bot)

end_x = p[0][-1]
end_w = X - end_x
end_n = int(end_w//D + 1)
end_d = float(end_w)/(end_n - 1)
for i in range(end_n - 1):
    if (i % 2 == 0) != top_to_bot:
        add_point(p, (end_x + i*end_d, -Y))
        add_point(p, (end_x + (i + 1)*end_d, -Y))
    else:
        add_point(p, (end_x + i*end_d, 0))
        add_point(p, (end_x + (i + 1)*end_d, 0))
if end_n % 2 == 1:
    top_to_bot = not top_to_bot
if top_to_bot:
    add_point(p, (X, -Y))
else:
    add_point(p, (X, 0))

##########################
f = plt.figure()
ax = f.gca()
# ax.add_patch(Rectangle((0, -Y), X, Y, fill=False, ec='k'))
for i in range(HNX):
    for j in range(HNY):
        ax.add_patch(Rectangle((HCX + i*HDX - HX/2., -HCY - j*HDY - HY/2.), HX, HY, fill=False, ec='b'))
ax.plot(p[0], p[1], 'r')
ax.set_xlim((-X/8., X*9/8.))
ax.set_ylim((-Y*9/8., Y/8.))
ax.set_aspect('equal', adjustable='box')

# generate_dxf(p[0], p[1],  linewidth=150e-6, save_filename='')