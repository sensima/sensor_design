from copy import copy
import numpy as np

class Shielding (object):
    def __init__(self, x=None, y=None, D=1, top_to_bot=None, vertical=True, plot_patches=None):
        self.x = [] if x is None else x
        self.y = [] if y is None else y
        self.D = D
        self.top_to_bot = top_to_bot
        self.vertical = vertical
        self.plot_patches = [] if plot_patches is None else plot_patches
        self.verify_lengths()
    
    @property
    def n(self):
        """The number of vertices in the shielding.
        """
        return len(self.x)
    
    def verify_lengths(self):
        """Verifies that the lengths of both the x and y coordinates are equal.
        If not, the longest of the two is troncated to the size of the other
        one.

        :return: True if the lengths are equal, False otherwise.
        """
        lx = len(self.x)
        ly = len(self.y)
        if lx != ly:
            if lx > ly:
                self.x = self.x[:ly]
            else:
                self.y = self.y[:lx]
            return False
        else:
            return True
    
    def copy(self):
        s2 = Shielding(
            x=copy(self.x),
            y=copy(self.y),
            D=self.D,
            top_to_bot=self.top_to_bot,
            vertical=self.vertical,
            plot_patches=copy(self.plot_patches)
        )
        return s2
    
    def get_bounding_box(self):
        """Determines the smallest rectangle that contains all vertices.

        :return: The coordinates of the rectangle in the form (bottom left
            corner, top right corner).
        """
        return ((min(self.x), min(self.y)), (max(self.x), max(self.y)))
    
    def get_width(self):
        """The width of the shielding"""
        return max(self.x) - min(self.x)
    
    def get_height(self):
        """The height of the shielding"""
        return max(self.y) - min(self.y)
    
    def get_dimensions(self):
        """The dimensions of the shielding (width and height).
        
        :return: The dimensions of the shielding in the form (width, height)
        """
        return (self.get_width(), self.get_height())

    def add_vertex(self, x, y=None, relative=False):
        """Append a new vertex to the shielding.

        :param x: The x coordinate of the new vertex, or a 

        :param y: The y coordinate of the new vertex, or None if using the ..
        mode

        :param relative: If True, the given coordinates will be treated as a
            distance from the last vertex instead of an absolute position
        
        :return: The object itself (to allow function chaining).
            (default: False).
        """
        if y is None:
            y = x[1]
            x = x[0]
        if relative:
            x += self.x[-1]
            y += self.y[-1]
        self.x.append(x)
        self.y.append(y)

        return self
    
    def add_shielding(self, o):
        """Appends another shielding to the object.
        
        :param o: The other shielding to add to the object.
        
        :return: The object itself (to allow function chaining).
        """
        self.x.extend(o.x)
        self.y.extend(o.y)
        if o.top_to_bot is not None:
            self.top_to_bot = o.top_to_bot
        
        return self

    def translate(self, x, y=None):
        """Translates the shielding by the given amount.

        :param x: The x value of the translation, or an indexable with [x, y] if
            y is None.

        :param y: The y value of the translation, or None if an indexable is
            given as the x parameter.

        :return: The object itself (to allow function chaining).
        """
        if y is None:
            y = x[1]
            x = x[0]
        for i in range(len(self.x)):
            self.x[i] += x
        for i in range(len(self.y)):
            self.y[i] += y
        for p in self.plot_patches:
            try:
                p.set_x(p.get_x() + x)
                p.set_y(p.get_y() + y)
            except:
                pass
        
        return self

    def __add__(self, o):
        res = self.copy()
        res += o
        return res
    
    def __iadd__(self, o):
        if isinstance(o, Shielding):
            self.add_shielding(o)
        elif isinstance(o, (tuple, list, np.ndarray)):
            self.add_vertex(o)
        return self
    
    def reverse_order(self):
        """Reverse the order of the vertices, the first one becoming last and
        vice-versa.

        :return: The object itself (to allow function chaining)
        """
        self.x = self.x[::-1]
        self.y = self.y[::-1]
        return self
    
    def remove_duplicated_vertices(self):
        """Removes all duplicated vertices, that is any vertex that is striclty
        equal to the one before it.
        
        :return: The object itself (to allow function chaining).
        """
        idx = []
        for i in range(len(self.x) - 1):
            if self.x[i + 1] == self.x[i] and self.y[i + 1] == self.y[i]:
                idx.append(i)
        self.x = np.delete(self.x, idx).tolist()
        self.y = np.delete(self.y, idx).tolist()

        return self
    
    def create_fill(self, dx, dy, x=None, y=None, inverse_start_direction=None, end=None, adapt_width=True, end_points=None, D=None, vertical=None):
        """Creates a rectangular patch of shielding (zig-zag style). Some
        parameters may be derived from the base shielding object.

        :param dx: The width of the rectangle. The value is sign sensitive.
            Positive (resp. negative) means the rectangle will be on the right
            (resp. left) of the reference corner.

        :param dy: The height of the rectangle. The value is sign sensitive.
            Positive (resp. negative) means the rectangle will be above (resp.
            under) the reference corner.

        :param x: x coordinate of the reference corner of the rectangle. If
            None, takes it from the last vertex of the shielding.

        :param y: y coordinate of the reference corner of the rectangle. If
            None, takes it from the last vertex of the shielding.

        :param inverse_start_direction: How the direction of the first segment
            is handled. If False, the first segment will start at the reference
            corner. If True, it will end at the reference corner instead. If
            None, the function will try to be smart and choose the right option
            based on the "top_to_bot" field of the base shielding object. This
            value is automatically handled by the add_fill and add_loop_fill
            functions.

        :param end: How the last vertex is handled:

            * 'opposite': The last vertex will be on the opposite side as the
                first one.

            * 'same': The last vertex will be on the same side as the first one.

            * 'top: The last vertex will be on the top side (for vertical
                shieldings).

            * 'bottom: The last vertex will be on the bottom side (for vertical
                shieldings).

            * 'right: The last vertex will be on the right side (for horizontal
                shieldings).

            * 'left: The last vertex will be on the left side (for horizontal
                shieldings).

            * None: The last vertex will be at the place that minimizes the
                segment to segment distance (adapt_width=True) or maximizes the
                shielding coverage (adapt_width=False).

            Also accepts any truncated version of the strings above (for
            example, 'b' and 'bot' are both equivalent to 'bottom').

        :param adapt_width: If True, the segment to segment distance may be
            increased for the shielding to cover the whole rectangle. If False,
            the segment to segment distance will be exactly the one specified at
            the cost of the shielding potentially not covering a part of the
            rectangle.

        :param end_points: Whether the add the end vertices to the generated
            rectangular shielding. Options are 'start', 'end', 'both' or None.

        :param D: The segment to segment distance (that is, the distance between
            two parallel segments). If None, taken from the base shielding
            object.

        :param vertical: Whether the zig-zag main direction is vertical (True)
            or horizontal (False). If None, taken from the base shielding
            object.

        :return: The newly generated rectangular patch of shielding (as a
            Shielding object).
        """
        if vertical is None:
            vertical = self.vertical
        if inverse_start_direction is None:
            if vertical:
                if dy == 0:
                    inverse_start_direction = False
                else:
                    inverse_start_direction = self.top_to_bot == (dy > 0)
            # else:
            #     inverse_start_direction = False
        if x is None:
            x = self.x[-1]
        if y is None:
            y = self.y[-1]
        if D is None:
            D = self.D
        if not vertical:
            fill = self.create_fill(dy, dx, 0, 0, inverse_start_direction=inverse_start_direction, end=end, adapt_width=adapt_width, end_points=end_points, D=D, vertical=True)
            fill2 = fill.copy()
            fill.x = fill2.y
            fill.y = fill2.x
            fill.translate(x, y)
            return fill
        if inverse_start_direction:
            return self.create_fill(dx, -dy, x, y + dy, inverse_start_direction=False, end=end, adapt_width=adapt_width, end_points=end_points, D=D, vertical=vertical)
        
        fill = Shielding(D=D)
        if end_points in ['start', 'both']:
            fill.add_vertex(0, 0)
        s = 1 if dy > 0 else (-1 if dy < 0 else 0)
        n = int(abs(dx)//D) # number of vertical lines that can fit
        if end is not None:
            if 'same'.startswith(end):
                if n % 2 == 0:
                    n -= 1
            elif 'opposite'.startswith(end):
                if n % 2 == 1:
                    n -= 1
            elif 'top'.startswith(end) or 'right'.startswith(end):
                if n % 2 != int(dy < 0):
                    n -= 1
            elif 'bottom'.startswith(end) or 'left'.startswith(end):
                if n % 2 == int(dy < 0):
                    n -= 1
        if n > 0:
            d = float(dx)/n if adapt_width else s*D
            for i in range(n):
                if i % 2 == 0:
                    fill.add_vertex(i*d, dy)
                    fill.add_vertex((i + 1)*d, dy)
                else:
                    fill.add_vertex(i*d, 0)
                    fill.add_vertex((i + 1)*d, 0)
        
            if end_points in ['end', 'both']:
                fill.add_vertex(n*d, ((n - 1) % 2)*dy)

        fill.translate(x, y)
        if dy != 0:
            fill.top_to_bot = n % 2 != int(dy < 0)
            if end_points in ['end', 'both']:
                fill.top_to_bot = not fill.top_to_bot

        return fill
    
    def add_fill(self, dx, dy, x=None, y=None, inverse_start_direction=None, end=None, adapt_width=True, end_points=None, D=None, vertical=None):
        """Creates and appends a rectangular patch of shielding (zig-zag style).
        Some parameters may be derived from the base shielding object.

        :param dx: The width of the rectangle. The value is sign sensitive.
            Positive (resp. negative) means the rectangle will be on the right
            (resp. left) of the reference corner.

        :param dy: The height of the rectangle. The value is sign sensitive.
            Positive (resp. negative) means the rectangle will be above (resp.
            under) the reference corner.

        :param x: x coordinate of the reference corner of the rectangle. If
            None, takes it from the last vertex of the shielding.

        :param y: y coordinate of the reference corner of the rectangle. If
            None, takes it from the last vertex of the shielding.

        :param inverse_start_direction: How the direction of the first segment
            is handled. If False, the first segment will start at the reference
            corner. If True, it will end at the reference corner instead. If
            None, the function will try to be smart and choose the right option
            based on the "top_to_bot" field of the base shielding object. This
            value is automatically handled by the add_fill and add_loop_fill
            functions.

        :param end: How the last vertex is handled:

            * 'opposite': The last vertex will be on the opposite side as the
                first one.

            * 'same': The last vertex will be on the same side as the first one.

            * 'top: The last vertex will be on the top side (for vertical
                shieldings).

            * 'bottom: The last vertex will be on the bottom side (for vertical
                shieldings).

            * 'right: The last vertex will be on the right side (for horizontal
                shieldings).

            * 'left: The last vertex will be on the left side (for horizontal
                shieldings).

            * None: The last vertex will be at the place that minimizes the
                segment to segment distance (adapt_width=True) or maximizes the
                shielding coverage (adapt_width=False).

            Also accepts any truncated version of the strings above (for
            example, 'b' and 'bot' are both equivalent to 'bottom').

        :param adapt_width: If True, the segment to segment distance may be
            increased for the shielding to cover the whole rectangle. If False,
            the segment to segment distance will be exactly the one specified at
            the cost of the shielding potentially not covering a part of the
            rectangle.

        :param end_points: Whether the add the end vertices to the generated
            rectangular shielding. Options are 'start', 'end', 'both' or None.

        :param D: The segment to segment distance (that is, the distance between
            two parallel segments). If None, taken from the base shielding
            object.

        :param vertical: Whether the zig-zag main direction is vertical (True)
            or horizontal (False). If None, taken from the base shielding
            object.

        :return: The object itself (to allow function chaining).

        """
        fill = self.create_fill(dx, dy, x, y, inverse_start_direction, end, adapt_width, end_points, D, vertical)
        self += fill
        return self

    def create_loop_fill(self, dx, dy, x=None, y=None, inverse_start_direction=None, adapt_width=True, D=None, allow_last=False, vertical=None):
        """Creates a rectangular patch of shielding, zig-zag style going back
        and forth to form a loop (not closed). Some parameters may be derived
        from the base shielding object.

        :param dx: The width of the rectangle. The value is sign sensitive.
            Positive (resp. negative) means the rectangle will be on the right
            (resp. left) of the reference corner.

        :param dy: The height of the rectangle. The value is sign sensitive.
            Positive (resp. negative) means the rectangle will be above (resp.
            under) the reference corner.

        :param x: x coordinate of the reference corner of the rectangle. If
            None, takes it from the last vertex of the shielding.

        :param y: y coordinate of the reference corner of the rectangle. If
            None, takes it from the last vertex of the shielding.

        :param inverse_start_direction: How the direction of the first segment
            is handled. If False, the first segment will start at the reference
            corner. If True, it will end at the reference corner instead. If
            None, the function will try to be smart and choose the right option
            based on the "top_to_bot" field of the base shielding object. This
            value is automatically handled by the add_fill and add_loop_fill
            functions.

        :param adapt_width: If True, the segment to segment distance may be
            increased for the shielding to cover the whole rectangle. If False,
            the segment to segment distance will be exactly the one specified at
            the cost of the shielding potentially not covering a part of the
            rectangle.

        :param D: The segment to segment distance (that is, the distance between
            two parallel segments). If None, taken from the base shielding
            object.

        :param allow_last: If false, the final edge of the rectangle is
            considered already covered by another shielding and will therefore
            be avoided by the shielding generated by this function.

        :param vertical: Whether the zig-zag main direction is vertical (True)
            or horizontal (False). If None, taken from the base shielding
            object.

        :return: The newly generated rectangular patch of shielding (as a
            Shielding object).
        """
        if vertical is None:
            vertical = self.vertical
        if inverse_start_direction is None:
            if vertical:
                if dy == 0:
                    inverse_start_direction = False
                else:
                    inverse_start_direction = self.top_to_bot == (dy > 0)
            # else:
            #     inverse_start_direction = False
        if x is None:
            x = self.x[-1]
        if y is None:
            y = self.y[-1]
        if D is None:
            D = self.D
        if not vertical:
            fill = self.create_loop_fill(dy, dx, 0, 0, inverse_start_direction=inverse_start_direction, adapt_width=adapt_width, D=D, allow_last=allow_last, vertical=True)
            fill2 = fill.copy()
            fill.x = fill2.y
            fill.y = fill2.x
            fill.translate(x, y)
            return fill
        if inverse_start_direction:
            return self.create_loop_fill(dx, -dy, x, y + dy, inverse_start_direction=False, adapt_width=adapt_width, D=D, allow_last=allow_last, vertical=vertical)
        
        fill = Shielding(D=D)
        s = 1 if dy > 0 else (-1 if dy < 0 else 0)
        h = (dy - s*D)/2.
        n = int(abs(dx)//D) # number of vertical lines that can fit
        n -= (n + int(not allow_last)) % 2
        if n > 0:
            d = float(dx)/n if adapt_width else s*D
            n -= int(not allow_last)
            for i in range(n):
                if i % 2 == 0:
                    fill.add_vertex(i*d, h)
                    fill.add_vertex((i + 1)*d, h)
                else:
                    fill.add_vertex(i*d, 0)
                    fill.add_vertex((i + 1)*d, 0)
            for i in range(n)[::-1]:
                if i % 2 == 0:
                    fill.add_vertex((i + 1)*d, dy - h)
                    fill.add_vertex(i*d, dy - h)
                else:
                    fill.add_vertex((i + 1)*d, dy)
                    fill.add_vertex(i*d, dy)

        fill.translate(x, y)
        if dy != 0:
            fill.top_to_bot = dy < 0

        return fill

    def add_loop_fill(self, dx, dy, x=None, y=None, inverse_start_direction=None, adapt_width=True, D=None, allow_last=False, vertical=None):
        """Creates and appends a rectangular patch of shielding, zig-zag style
        going back and forth to form a loop (not closed). Some parameters may be
        derived from the base shielding object.

        :param dx: The width of the rectangle. The value is sign sensitive.
            Positive (resp. negative) means the rectangle will be on the right
            (resp. left) of the reference corner.

        :param dy: The height of the rectangle. The value is sign sensitive.
            Positive (resp. negative) means the rectangle will be above (resp.
            under) the reference corner.

        :param x: x coordinate of the reference corner of the rectangle. If
            None, takes it from the last vertex of the shielding.

        :param y: y coordinate of the reference corner of the rectangle. If
            None, takes it from the last vertex of the shielding.

        :param inverse_start_direction: How the direction of the first segment
            is handled. If False, the first segment will start at the reference
            corner. If True, it will end at the reference corner instead. If
            None, the function will try to be smart and choose the right option
            based on the "top_to_bot" field of the base shielding object. This
            value is automatically handled by the add_fill and add_loop_fill
            functions.

        :param adapt_width: If True, the segment to segment distance may be
            increased for the shielding to cover the whole rectangle. If False,
            the segment to segment distance will be exactly the one specified at
            the cost of the shielding potentially not covering a part of the
            rectangle.

        :param D: The segment to segment distance (that is, the distance between
            two parallel segments). If None, taken from the base shielding
            object.

        :param allow_last: If false, the final edge of the rectangle is
            considered already covered by another shielding and will therefore
            be avoided by the shielding generated by this function.

        :param vertical: Whether the zig-zag main direction is vertical (True)
            or horizontal (False). If None, taken from the base shielding
            object.

        :return: The object itself (to allow function chaining).
        """
        fill = self.create_loop_fill(dx, dy, x, y, inverse_start_direction, adapt_width, D, allow_last, vertical)
        self += fill
        return self
    
    def plot(self, fig=None, ax=None):
        """Plots a graphical representation of the shielding.

        :param fig: A maplotlib figure object to use as the container. Ignored
            if ax is not None. If both fig and ax are None, a new figure object
            will be generated.

        :param ax: A matplotlib axis object to use as the container.
        """
        import matplotlib.pyplot as plt

        if ax is None:
            if fig is None:
                fig = plt.figure()
            ax = fig.gca()
        for patch in self.plot_patches:
            ax.add_patch(patch)
        ax.plot(self.x, self.y, 'r')
        ax.set_xlim((min(self.x) - self.get_width()/8., max(self.x) + self.get_width()/8.))
        ax.set_ylim((min(self.y) - self.get_height()/8., max(self.y) + self.get_height()/8.))
        ax.set_aspect('equal', adjustable='box')
    
    def generate_dxf(self, dxf=None, linewidth=0.15, layer='top', save_filename=None):
        """Generates or updates a dxf document with the shielding tracks. If
        save_filename is given, also saves the document to disk.

        :param dxf: An existing dxf document to put the shielding into. If None,
            a new dxf document will be created.

        :param linewidth: Track width [mm] (default: 150 um).

        :param layer: Name of the layer to put the shielding into.

        :param save_filename: Full filename used to save the generated dxf
            (optional, no save action performed if not given).

        :return: The generated or udpated dxf document.
        """
        if dxf is None:
            try:
                import ezdxf
                dxf = ezdxf.new('R2000')
            except:
                print('Critical error: ezdxf module could not be imported')
                return None

        msp = dxf.modelspace()
        dxfattribs = {
            'const_width': linewidth,
            'layer': layer
        }

        # for i in range(len(self.x) - 1):
            # msp.add_lwpolyline([(self.x[i], self.y[i]), (self.x[i+1], self.y[i+1])], dxfattribs=dxfattribs)
        msp.add_lwpolyline([(self.x[i], self.y[i]) for i in range(len(self.x))], dxfattribs=dxfattribs)
        
        if save_filename is not None:
            dxf.saveas(save_filename)

        return dxf

def generate_shielding_with_hole_array(dimensions, hole_center, hole_distance, hole_dimensions, hole_grid_size, D, start_top=True, end=None, vertical=True):
    from matplotlib.patches import Rectangle

    x, y = dimensions
    hcx, hcy = hole_center
    hdx, hdy = hole_distance
    hx, hy = hole_dimensions
    hnx, hny = hole_grid_size

    if not vertical:
        shielding = generate_shielding_with_hole_array((y, x), (hcy, hcx), (hdy, hdx), (hy, hx), (hny, hnx), D, start_top, end)
        s2 = shielding.copy()
        shielding.x = [-z for z in s2.y]
        shielding.y = [-z for z in s2.x]

    patches = []
    patches.append(Rectangle((0, -y), x, y, fill=False, ec='k'))
    for i in range(hnx):
        for j in range(hny):
            patches.append(Rectangle((hcx + i*hdx - hx/2., -hcy - j*hdy - hy/2.), hx, hy, fill=False, ec='b'))
    
    if not vertical:
        shielding.plot_patches = patches
        return shielding
    
    shielding = Shielding(D=D, top_to_bot=start_top, plot_patches=patches)
    shielding.add_fill(hcx - hx/2., -y, 0, 0, end_points='start')

    def add_vertical_segment_around_holes():
        if shielding.top_to_bot:
            shielding.add_loop_fill(hx, hy/2. - hcy, None, 0)
        else:
            shielding.add_loop_fill(hx, y - hcy - (hny -  1)*hdy - hy/2.)

        js = range(hny - 1)
        if not shielding.top_to_bot:
            js = js[::-1]
        for j in js:
            shielding.add_loop_fill(hx, hy - hdy, None, -hcy - j*hdy -hy/2.)
        
        if shielding.top_to_bot:
            shielding.add_fill(hx, y - hcy - (hny -  1)*hdy - hy/2., None, -y, end='same')
        else:
            shielding.add_fill(hx, hy/2. - hcy, None, 0, end='same')

    for i in range(hnx - 1):
        add_vertical_segment_around_holes()
        shielding.add_fill(hdx - hx, -y, None, 0)

    add_vertical_segment_around_holes()
    shielding.add_fill(x - hcx - (hnx - 1)*hdx - hx/2., -y, None, 0, end=end, end_points='end')
    
    return shielding
