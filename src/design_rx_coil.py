# -*- coding: utf-8 -*-
"""

Collection of useful scripts, fucntions, methods for the design of air wounded coils.

__author__      = "Enrico Gasparin"

2020-06-30

Optimised designa of a RX coil arguments. The script will find the macimum inductance satisfying the constraints

ARG1: targeted coils self-resonant frequency in kHz 
ARG2: flag, indicate if you would like to choose a parallel capacitance to tune a resonance at the working frequency (1:True 0:False)
ARG3: working frequency in kHz
ARG4: number of layers
ARG5: the minimum iratio factor, i.e. the ratio among the current circulation in the inductor and those in the parallel capacitance. In the case the
    receiving coil is not resonant, this capacitance represents the parasitic capacitance. 
    In principle, it is another wa to set a limit to the self-resonance.

Example
python .\design_rx_coil.py coils_geom\VB46R_F.json 300 0 130 13 13

"""
from logging.handlers import RotatingFileHandler
import logging
# setup rotating file logger
logger = logging.getLogger()
hdlr = RotatingFileHandler(
    "./_logs/coils_calc.log", mode="a", maxBytes=5 * 1024 * 1024, backupCount=2, encoding=None, delay=0
)
logFormatter = logging.Formatter("%(asctime)s :: %(levelname)s :: %(message)s")
hdlr.setFormatter(logFormatter)
logger.addHandler(hdlr)
# set logger level
logger.setLevel(logging.DEBUG)
logger.setLevel(logging.INFO)
logger.setLevel(logging.ERROR)

logToConsole = True
# additional setup, to log to console too - more for debug or terminal operations
if logToConsole:
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    logger.addHandler(consoleHandler)

import numpy as np
import scipy.constants as cnst
from scipy import signal


import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

import json
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
import sys

import pancake_coils_draw as pcd

import pancake_coils_calc as pcc

import passive_components
# I do suppress warnings....
import warnings
warnings.filterwarnings("ignore")

if __name__ == "__main__":

    # base design for the dimensioning
    filename = dir_path + "/" + sys.argv[1] #'VB4_square.json'

    with open(filename) as json_file:
        coil = json.load(json_file)

    # self-resonance frequency in kHz
    selfres_freq_khz = float(sys.argv[2])
    selfres_freq = selfres_freq_khz*1e3

    # does the coil use a parallel external capacitor to be resonant?
    is_resonant_rx = bool(int(sys.argv[3]))
    print(is_resonant_rx)
    # working resonant frequency
    work_freq_khz = float(sys.argv[4])
    work_freq = work_freq_khz*1e3

    # coil number of layers
    layers = int(sys.argv[5])

    try:
        aimed_iratio  = int(sys.argv[6])
    except:
        aimed_iratio = 0.0
        print("---> assuming aimed iratio to its default value, aimed_iratio={:.2f}".format(aimed_iratio))


    # get out some relevant parameter
    a = coil['a']
    b = coil['b']


    # given the dimensions, I do iterate on the possible combinations
    nn = 30#10#30 
    cu_w_list = np.linspace(150e-6,2000e-6,nn) 
    cu_d_list = np.linspace(150e-6,4000e-6,nn) 

    fres_grid = np.zeros((cu_w_list.size,cu_d_list.size)) # resonant frequency
    fself_grid = np.zeros((cu_w_list.size,cu_d_list.size)) # resonant frequency
    frl_grid = np.zeros((cu_w_list.size,cu_d_list.size)) # electric RL pole
    l_grid = np.zeros((cu_w_list.size,cu_d_list.size)) # inductance
    r_grid = np.zeros((cu_w_list.size,cu_d_list.size)) # resistance
    c_grid = np.zeros((cu_w_list.size,cu_d_list.size)) # capacitance selected for the resonance excitation
    cself_grid = np.zeros((cu_w_list.size,cu_d_list.size)) # capacitance related to the self resonance
    q_grid = np.zeros((cu_w_list.size,cu_d_list.size)) # quality factor
    i_grid = np.zeros((cu_w_list.size,cu_d_list.size)) # current
    phi_grid = np.zeros((cu_w_list.size,cu_d_list.size)) # flux
    n_grid = np.zeros((cu_w_list.size,cu_d_list.size)) # number of turns
    zeros_grid = np.zeros((cu_w_list.size,cu_d_list.size))
    iratio_grid = np.zeros((cu_w_list.size,cu_d_list.size)) # current ration among inductor and self capacitance

    # calculate all possible options 
    maxiter = float(cu_w_list.size * cu_d_list.size)
    iter = 0

    for j, cu_w in enumerate(cu_w_list):
        for k, cu_d in enumerate(cu_d_list):


            logger.info("**** iter {:d}".format(iter))
# 
            
            
            # with constraint on the hollow center
            if ('a0' in coil.keys()) and ('b0' in coil.keys()):
                a0 = coil['a0']
                b0 = coil['b0']
                nmax = int(np.floor(np.min([0.5*(a - a0),0.5*(b - b0)])/(cu_w + cu_d))) -1
            else:
                # number of turns without any constraint
                nmax = int(np.floor(0.5*np.min([a,b])/(cu_w + cu_d))) -1

            logger.info("max number of turns {:.2f}".format(nmax))
        
            coil['n'] = nmax  # set maximum number of turns
            coil['cu_w'] = cu_w # set copper width
            coil['cu_d'] = cu_d # set copper clearancec
            coil['layers'] = layers

            L = pcc.calculate_self_inductance(coil)
            Cself = pcc.calculate_self_capacitance(coil, shielding=True)
            R = pcc.calculate_resistance(coil)

            iter+=1


            f_rl = (R/L)/(2*np.pi)
            logger.info("-> f_rl {:.2f} kHz".format(f_rl*1e-3))


            
            frl_grid[j][k] = f_rl
            l_grid[j][k] = L
            r_grid[j][k] = R
            n_grid[j][k] = nmax

            

            # frsp = pcc.calculate_bode_diagrams(R, L, C, Vs = max_v, Rg = Rg, w = freq*2*np.pi)
            if is_resonant_rx:
                # equivalent capacitor to have a resonant receiver
                ceq = 1/(L*(2*np.pi*work_freq)**2)

                # calculate the value of the capacitance that we should add in paralle, accounting for the parasitic capacitance
                cx = ceq - Cself

                Cpar = passive_components.capacitors.find_nearest_value(cx)

                c_grid[j][k] = Cpar

                C = Cpar + Cself
            else:
                C = Cself

            cself_grid[j][k] = Cself

            # calculate the resonance frequency on the basis of the overall parallel capacitance
            f_res = 1/(2*np.pi*np.sqrt(L*C))
            logger.info("-> f_res {:.2f} kHz".format(f_res*1e-3))
            fres_grid[j][k] = f_res
            fself_grid[j][k] = 1/(2*np.pi*np.sqrt(L*Cself))


            # as well the quality factor
            q_grid[j][k] = 1/R*np.sqrt(L/C)


            # take the chance to calculate the current ratio among the inductor and the parallel capacitor
            frsp = pcc.calculate_coil_freq_resp(R, L, C, w=work_freq*2*np.pi)
            iratio_grid[j][k] = pcc.get_vals_at_specific_freq(frsp,work_freq)['iratio']



            sys.stdout.write('\r...progress {:.2f}%'.format(iter/maxiter*100.))
            sys.stdout.flush()


    # make a selection of the best options
    # (1): frequency constraint: let's say that I target the working frequency within a ...1% window?
    fwdw_prc = 1/100.
    
    if is_resonant_rx:
        # let's say that I target the working frequency 
        sel_fres = (fres_grid >= work_freq*(1-fwdw_prc)) & (fres_grid <= work_freq*(1+fwdw_prc)) & (fself_grid >= selfres_freq*(1-fwdw_prc))
    else:
        # otherwise I just want to work below the self-resonance that I specified
        sel_fres = (fres_grid >= selfres_freq*(1-fwdw_prc))

    

    # selection of the current ratios satisfying the constraint
    sel_iratio = iratio_grid >= aimed_iratio

    # global mask for selection
    final_mask = sel_fres & sel_iratio

    if not np.any(final_mask) :
        raise Exception("No solution found") 


    # pick up, as a proposition, the combination within the selection that maximises the inductance
    selected_l = np.zeros((cu_w_list.size,cu_d_list.size))
    selected_l[final_mask] = l_grid[final_mask] # find final proposed inductance
    
    opt_sel = np.where(selected_l == np.amax(selected_l))
    opt_x = opt_sel[1]
    opt_y = opt_sel[0]
 

    # ## final electrical parameters
    Cpar = c_grid.T[opt_x,opt_y][0]
    Cself = cself_grid.T[opt_x,opt_y][0]
    R = r_grid.T[opt_x,opt_y][0]
    L = l_grid.T[opt_x,opt_y][0]
    # also overwrite these parameters to the "coil" structure for future use in this script
    coil["cu_w"] = cu_w_list[opt_y][0]
    coil["cu_d"] = cu_d_list[opt_x][0]
    coil["n"] = int(n_grid.T[opt_x,opt_y][0])

    # color map for the mask
    cmap_mask = plt.cm.get_cmap("seismic")


    # plotting
    note = ''
    if is_resonant_rx:
        note = 'res'
    
    note += "L{:d}T{:d}".format(layers, int(coil["cu_t"]*1e6))

    fnum = 1001
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    plt.title('Resonance frequency - {:s}'.format(coil['sensor']))
    X, Y = np.meshgrid(cu_w_list*1e3, cu_d_list*1e3)
    ax.plot_surface(X, Y, fres_grid.T*1e-6, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    ax.scatter(X, Y, zeros_grid, c = np.reshape(sel_fres.T, sel_fres.size), cmap= cmap_mask, edgecolor='none', alpha=0.25)
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('fres [MHz]')
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    fnum = 1003
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, l_grid.T*1e6, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('L [uH]')
    plt.title('Inductance - {:s}'.format(coil['sensor']))
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    fnum = 1004
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, r_grid.T, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('R [ohm]')
    plt.title('Resistance - {:s}'.format(coil['sensor']))
    ax.set_zlim(0, 100)
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    fnum = 1007
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, n_grid.T, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    ax.scatter(X, Y, zeros_grid, c = np.reshape(final_mask.T, final_mask.size), edgecolor='none',cmap= cmap_mask,  alpha=0.25) #final result
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('n [1]')
    plt.title('Number of turns - {:s}'.format(coil['sensor']))
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    fnum = 1008
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, q_grid.T, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    ax.scatter(X, Y, zeros_grid, c = np.reshape(final_mask.T, final_mask.size), edgecolor='none',cmap= cmap_mask,  alpha=0.25) #final result
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('Q [1]')
    plt.title('Quality Factor - {:s}'.format(coil['sensor']))
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    fnum = 1009
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, iratio_grid.T, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    ax.scatter(X, Y, zeros_grid, c = np.reshape(final_mask.T, final_mask.size), edgecolor='none',cmap= cmap_mask,  alpha=0.25) #final result
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('iratio [1]')
    plt.title('iratio - {:s}'.format(coil['sensor']))
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')







    fnum = 1010
    plt.figure(fnum)
    plt.scatter(X, Y, c = final_mask.T, cmap= cmap_mask, edgecolor='none', alpha=0.25)
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    # print(cu_w_list[opt_x][0]*1.0e3, cu_d_list[opt_y][0]*1.0e3, int(n_grid.T[opt_x,opt_y][0]) )
    # print(l_grid.T)
    str_plt = "L={:.2f}uH R={:.2f}ohm C={:.2f}nF Iratio={:.2f} Q={:.2f} Fres={:.2f}MHz Fself={:.2f}MHz \n cu_w={:.2f}mm cu_d={:.2f}mm n={:d} l={:d}".\
    format(l_grid.T[opt_x,opt_y][0]*1e6, r_grid.T[opt_x,opt_y][0], c_grid.T[opt_x,opt_y][0]*1e9, iratio_grid.T[opt_x,opt_y][0],q_grid.T[opt_x,opt_y][0], fres_grid.T[opt_x,opt_y][0]*1e-6, fself_grid.T[opt_x,opt_y][0]*1e-6, cu_w_list[opt_y][0]*1.0e3, cu_d_list[opt_x][0]*1.0e3, int(n_grid.T[opt_x,opt_y][0]), int(layers) ) 
    plt.title('Final result {:s} @ {:.2f}kHz '.format(coil['sensor'], work_freq*1e-3) + '\n' + str_plt, fontsize=10)
    plt.plot(cu_w_list[opt_y]*1e3, cu_d_list[opt_x]*1e3,'*r',markersize=15)
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    fnum = 1
    pcd.draw_coil(coil)
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    # plot a bode diagram of the coil
    frsp = pcc.calculate_coil_freq_resp(R, L, Cself, Cr=Cpar)

    fnum = 100
    plt.figure(fnum)
    idx = (np.abs(frsp['f']- work_freq)).argmin() #find index to ha
    plt.semilogx(frsp['f'], frsp['ampl_z'], label='coil impedance {:.2f}ohm@{:.2f}kHz'.format(frsp['ampl_z'][idx],work_freq*1e-3))    # Bode magnitude plot
    plt.plot(frsp['f'][idx], frsp['ampl_z'][idx],'*r',markersize=5)
    plt.grid()
    plt.legend()
    plt.title('Final result {:s} @ {:.2f}kHz '.format(coil['sensor'], work_freq*1e-3) + '\n' + str_plt, fontsize=10)
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    fnum = 101
    plt.figure(fnum)
    idx = (np.abs(frsp['f']- work_freq)).argmin() #find index to ha
    plt.semilogx(frsp['f'], frsp['ampl_ic'], label='Ic : parasitic capacitor current ratio')   # Bode magnitude plot
    plt.plot(frsp['f'][idx], frsp['ampl_ic'][idx],'*r',markersize=5)
    plt.semilogx(frsp['f'], frsp['ampl_icr'], label='Icr : resonant capacitor current ratio')   # Bode magnitude plot
    plt.plot(frsp['f'][idx], frsp['ampl_icr'][idx],'*r',markersize=5)
    plt.semilogx(frsp['f'], frsp['ampl_il'], label='Il : inductor-resistor current ratio')   # Bode magnitude plot

    # coil/parasitic capitance currents ratio at the working frequency
    iratio =  frsp['ampl_il'][idx]/frsp['ampl_ic'][idx]

    plt.plot(frsp['f'][idx], frsp['ampl_il'][idx],'*r',markersize=5)
    plt.text(1, 1.2,"curr. ratio Il/Ic = {:.2f}@{:.2f}kHz".format(iratio,work_freq*1e-3))
    plt.ylim([0, 6])
    plt.grid()
    plt.legend()
    plt.title('Final result {:s} @ {:.2f}kHz '.format(coil['sensor'], work_freq*1e-3) + '\n' + str_plt, fontsize=10)
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')



    with open( dir_path + "/figs/{:s}_{:s}_out.json".format(coil['sensor'],note), 'w') as outfile:
        json.dump(coil, outfile)


    plt.show()