'''
2020.05.04
very simple script for the design of an rlc filter.
useful to filter the higher armonics

Enrico Gasparin
'''


from scipy import signal
import matplotlib.pyplot as plt
import numpy as np



if __name__ == "__main__":

    # generator resistance
    R = 50.0
    
    # parallel capacitance
    C = 10.0e-9

    # inductor
    L = 78.0e-6
    RL = 34.0 # intrinsic coil resistance
    CL = 746e-12 # parasitic capacitance of the coil


    n = [(L*CL*R + L*C*R), (CL*RL*R + C*RL*R + L), (RL+R)]
    d = [(L*CL + L*C), (CL*RL + C*RL), 1]

    # n = [0,0,10]
    # d = [0,0,1]

    s1 = signal.lti(n, d)
    w = np.linspace(1,2e6*2*np.pi,1e5)
    w, mag, phase = signal.bode(s1,w )

    f = w/(2*np.pi)
    ampl = 10**(mag/20.0)

    plt.figure()
    plt.semilogx(f, ampl)    # Bode magnitude plot
    plt.grid()

    plt.figure()
    plt.semilogx(f, phase)  # Bode phase plot
    plt.grid()

    plt.show()



