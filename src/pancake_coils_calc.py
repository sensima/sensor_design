# -*- coding: utf-8 -*-
"""

Collection of useful scripts, fucntions, methods for the design of air wounded coils.

__author__      = "Enrico Gasparin"

2020-02-25


# coil construction
cu_w = 250e-6   #copper width
cu_d = 250e-6   #copper distance
cu_t = 35e-6    #copper thicknes
layers = 2
layers_d = 0.1e-3 # distance among layers
layers_skewing = 1 #indicates if layers are displaced by half the track size to reduce the self capacitance

# coil interconnection
line_w = cu_w
line_d = cu_d
line_length = 5e-2

# electric properties
epsilon_r = 4.7 # good approximation for FR4EEE

# equivalent lenght of the trace

n = 10    
# case (1) : rectangular/square coil
geometry = "rectangular"
a = 300e-3
b = 40e-3

# case (2) : spira coil
geometry = "spiral"
r = 400e-3 #outer radius


"""
from logging.handlers import RotatingFileHandler
import logging
# setup rotating file logger
# 
logger = logging.getLogger(__name__)


import numpy as np
import scipy.constants as cnst
from scipy import signal

import matplotlib.pyplot as plt

import json
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
import sys

import pancake_coils_draw
# I do suppress warnings....
import warnings
warnings.filterwarnings("ignore")
#======================================================================


def get_layer_distance(layer):

    # I do anticipate the layers distance, I take as reference tha layer stackup of EUROCIRCUITS (common..)
    if layer == 1 or layer == 2:
        layers_d = 1.55e-3
    elif layer == 3 or layer == 4:
        layers_d = 0.6e-3
    elif layer == 5 or layer == 6:
        layers_d = 0.3e-3
    elif layer == 7 or layer == 8:
        layers_d = 0.2e-3
    else:
        layers_d = 0.1e-3
        print("---> WARNING: assuming layer distance to a default small value: {:.2f}".format(layers_d))
        # raise Exception("number of layers is not supported") 

    return layers_d

#----------------------------------------------------------------------
def calculate_self_capacitance(p, out=None, shielding = True):
    '''
    calculation fo the self capacitance, based on static capacitance model 
    ( W. T. Duerdoth, “Equivalent capacitances of transformerwindings,” WirelessEng., vol. 23, pp. 161–167, Jun. 1946.)

    :param p: coil parameters (dict)
    :param out: (optional) if a dict is passed, it will be completed with the intermediary and more detailed results (i.e. those in the logs; see below for fields).
    :param shielding: (optional) assumes the presence of shielding (it accounts as a second layer). In general I recommend to leave this option activated as also models the presence of any external grounding.

    (Using Transformer Parasitics for Resonant Converters—A Review of the Calculation of the Stray Capacitance of Transformers)

    Enrico Gasparin

    .. note:: Fields for the optional output in out

       coil
          input coil parameters (copy of p)

       Cs1
          Side to side copper traces

       Cs2
          Fringe field copper traces

       Cs3
          Layer to layer traces

       Cs23
          Cummulative sum of Cs2 and Cs3 if relevant (to be checked)

       Cs4
          Connecting tracks

       Cs
          Estimated overall self-capacitance (Cs = Cs1 + Cs23 + Cs4)

    '''
    if not isinstance(out,dict): out = {} # makes all the other statements smaller but no effect ouside if out is not a dict!
    out['coil'] = p.copy()
    
    # coil construction
    cu_w = p['cu_w']   #copper width
    cu_d = p['cu_d']   #copper distance
    cu_t = p['cu_t']    #copper thicknes
    layers = p['layers']
    layers_d = p.get('layers_d')
    if layers_d is None:
        layers_d = p.setdefault('layers_d',get_layer_distance(layers)) #p['layers_d'] # distance among layers
    if 'layers_skewing' in p.keys():
        layers_skewing = p['layers_skewing'] #indicates if layers are displaced by half the track size to reduce the self capacitance
    else:
        layers_skewing = False


    # coil interconnection
    line_w = p['line_w']
    line_d = p['line_d']
    line_length = p['line_length']

    # electric properties
    try:
        epsilon_r = p['epsilon_r'] # good approximation for FR4
    except:
        epsilon_r = 4.7# good approximation for FR4

    

    # geometry
    n = p['n']  # turns
    geometry = p['geometry']
    a = p['a']
    b = p['b']
    r = p['r'] #outer radius -- for spiral only

    length = 0
    
    if geometry == "rectangular":
        for j in range(n):
            length += 2*((a - (n-j)*(cu_d + cu_w)*2) + 2*(b - (n-j)*(cu_d + cu_w)*2))
   
    if geometry == "spiral":
        for j in range(n):
            length += 2*np.pi*(r - j*(cu_d + cu_w)*2)


    # single layer pancake coil

    # consider the parasitic capacitance of the paralle lines

    s1 = cu_t*length # side of the coil
    s2 = cu_w*length # top of the coil
    
    Cs1 = cnst.epsilon_0*epsilon_r*s1/cu_d
    logger.info("Side to side copper traces Cs1: {:.2f} pF".format(Cs1/1e-12))
    out['Cs1'] = Cs1

    # need to consider as well the superimposing conduictors...
    Cs2 = 0
    Cs3 = 0
    
    # if layers > 1 or shielding:
    ''' multiple layers '''

        

    # capacitance contribution, half FR4 and half in the air
    Cs2 = 0.5*(cnst.epsilon_0*epsilon_r + cnst.epsilon_0)*s2/(0.5*np.pi*cu_d)#use the arc approximation for the path length
    logger.info("Fringe field copper traces Cs2: {:.2f} pF".format(Cs2/1e-12))
    out['Cs2'] = Cs2
    Cs23 = Cs2

    # this way of calculating assumes perfect overlaps...
    # on the two layers.

    # approximation of "parallel plates"
    s = length*cu_w
    if layers_skewing == True:   #I dropped this skewing approximation is useless

        if (layers-1):
            # account that I cannot do a perfect skwwing when I have more than 2 layers`!!
            skew_factor = 0.5/(layers/2.0)
        else: 
            skew_factor = 0.5

        logger.info("skew factor for capacitive coupling: {:.2f} ".format(skew_factor))
        distance = np.sqrt(layers_d**2 + (cu_d*skew_factor)**2) 
    else:
        distance = layers_d
    # distance = layers_d

    Cs3_single_pair = cnst.epsilon_0*epsilon_r*s/(distance)
    Cs3_single_pair_wo_skewing = cnst.epsilon_0*epsilon_r*s/(layers_d)
    
    if (layers-1):
        Cs3 += Cs3_single_pair
        for sl in range( int(0.5*layers) ):
            Cs3 += Cs3_single_pair/(2*(sl+1)) # I asssume the self-capacitance is given only by a single layer coupling, in the other layers I assume I am not able to do skewing
            # print (sl,  Cs3_single_pair_wo_skewing/(2*(sl+1)))
    Cs5 = 0
    if shielding:
        # 
        # logger.info("calculate_self_capacitance: shielding accounted")
        shielding_skweing_effectivness_factor = 0.5 #assume that I have a certain skewing

        # first order approximation, assumes a factor, I should normally do a shielding which does not cover one by one all the tracks...
        Cs5 = Cs3_single_pair*shielding_skweing_effectivness_factor #account for the shielding
        logger.info("Stray capacitance to shielding Cs5: {:.2f} pF, assuming a factor {:.2f}".format(Cs5/1e-12, shielding_skweing_effectivness_factor))

        logger.info("Layer to layer traces Cs3: {:.2f} pF ({:.2f} times Cs1, {:.2f} times Cs2 and {:.2f} times Cs5)".format(Cs3/1e-12, Cs3/Cs1, Cs3/Cs2, Cs3/Cs5))
    else:
        logger.info("Layer to layer traces Cs3: {:.2f} pF ({:.2f} times Cs1, {:.2f} times Cs2)".format(Cs3/1e-12, Cs3/Cs1, Cs3/Cs2))

    out['Cs3'] = Cs3

    Cs23 += Cs3 

    # else:
    #     ''' single layer '''
    #     # I do observe that there is a minor influence of such effect when we have a single layer - assuming only contribution in the air
    #     Cs2 = 0.5*cnst.epsilon_0*s2/(0.5*np.pi*cu_d) #(np.pi*0.5*(cu_d)) #use the arc approximation for the path length
    #     logger.info("Fringe field copper traces: {:.2f} pF".format(Cs2/1e-12))
    #     out['Cs2'] = Cs2
    #     Cs23 = Cs2

    # add the contribution for the interconnecting lines
    Cs4 = cnst.epsilon_0*epsilon_r*(line_w * line_length)/(line_d)
    logger.info("Connecting tracks: {:.2f} pF".format(Cs4/1e-12))
    out['Cs23'] = Cs23
    out['Cs4'] = Cs4
    out['Cs5'] = Cs5

    Cs = Cs1 + Cs23 + Cs4 + Cs5

    logger.info("Estimated overall self-capacitance: {:.2f} pF".format(Cs/1e-12))
    out['Cs'] = Cs

    return Cs


#----------------------------------------------------------------------
def calculate_self_inductance(p, out=None):
    '''
    Calculation of self inductance of current path based on partial loop inductances.
    A reference to the mathematical modelling can be found in  "Inductance: Loop and Partial, Clayton R. Paul, Wiley 2009"

    The rectangular pancake coils are approximated as a series of closed rectangular current paths with the same center.
    Similary, the spiral pancake coils are modelled as a series of concentrical circles. (this method is more accurate than what resulting using the Wheeler formula)

    :param p: is a dictionary containing the parameters
    :param out: (optional) if a dict is passed, it will be completed with the intermediary and more detailed results (i.e. those in the logs; see below for fields).

    Enrico Gasparin

    .. note:: Fields for the optional output in out

       coil
          input coil parameters (copy of p)

       Ltot
          Estimated overall self-inductance (H)

       L_geometry
          (string) type of geometry used for calculation

       L_is_geometry_spiral
          (bool) If True, means that "spiral inductance calculated with loops... and not with wheeler"

       Stot
          **(GS 2021-04-22: wrong)** Global cumulative surface (m2)

    '''

    if not isinstance(out,dict): out = {} # makes all the other statements smaller but no effect ouside if out is not a dict!
    out['coil'] = p.copy()
    
    # coil construction
    cu_w = p['cu_w']   #copper width
    cu_d = p['cu_d']   #copper distance
    cu_t = p['cu_t']    #copper thicknes
    layers = p['layers']
    layers_d = p.get('layers_d')
    if layers_d is None:
        layers_d = p.setdefault('layers_d',get_layer_distance(layers)) #p['layers_d'] # distance among layers

    # coil interconnection
    line_w = p['line_w']
    line_d = p['line_d']
    line_length = p['line_length']

    # electric properties
    epsilon_r = p['epsilon_r'] # good approximation for FR4

    # geometry
    n = p['n']  # turns
    geometry = p['geometry']
    a = p['a']
    b = p['b']
    r = p['r'] #outer radius -- for spiral only


    # calculate the partial inductance
    rw = cu_w




    tot_air_ind = 0
    Ltot = 0 # total inductance
    Stot = 0 # the total surface of every loop
    # for nl_id in range(layers):

    copper_surface = 0

    for nl_id in range(layers):    
        for j in range(n+1): # number of turns (+1, because l and w must equal a and b )
            for i in range(n-j +1 ):    # account for the inner loops
                if geometry == "rectangular":
                    l = a - (n-j)*(cu_d + cu_w)*2
                    w = b - (n-j)*(cu_d + cu_w)*2
                    length = 2*l+2*w
                    # Stot += l*w
                    

                if geometry == "spiral":
                    length = 2*np.pi*(r - j*(cu_d + cu_w)*2)
                    l = length/4.0
                    w = length/4.0
                    # l = a - (n-j)*(cu_d + cu_w)*2
                    # w = b - (n-j)*(cu_d + cu_w)*2
                    
                if i==0:
                    # The equvialent surface sholud be only calculated once per each loop
                    Stot += l*w
            
                if(j==0):
                    copper_surface += length*cu_w

                # formula taken from page 125. valid fr l,w >> rw, normally should be always the case
                # Lloop = cnst.mu_0/(np.pi)*(w*np.log(2*w/rw) -w -w*np.arcsinh(w/l) + np.sqrt(w**2 + l**2) - l \
                # + l*np.log(2*l/rw) - l -l*np.arcsinh(l/w) + np.sqrt(l**2 + w**2) - w)

                # more generic formula
                Lloop = cnst.mu_0/(np.pi)* ( -(l-rw)*np.arcsinh(( l-rw )/( w-rw )) 
                - (w-rw)*np.arcsinh(( w-rw )/( l-rw ))
                + (l-rw)*np.arcsinh(( l-rw )/( rw ))
                + (w-rw)*np.arcsinh(( w-rw )/( rw ))
                + rw*np.arcsinh(( rw )/( w-rw ))
                + rw*np.arcsinh(( rw )/( l-rw ))
                + 2*np.sqrt( (l-rw)**2 + (w-rw)**2 ) - 2*np.sqrt( (w-rw)**2 + (rw)**2 )
                - 2*np.sqrt( (l-rw)**2 + (rw)**2 ) - 2*rw*np.log(1 + np.sqrt(2)) + 2*np.sqrt(2)*rw
                   )



                

                # update inductance
                Ltot += Lloop*(1 +nl_id) # account also for other layers


    # distance for the calculation of the proximity effect ration
    # the exact formula for circular section conductors is s = cu_d + rw
    s = cu_d + rw #+ rw #-0.5*rw # distance among center to center conductors
    # correction factor due to the proximity effect (page 161)
    enricos_correction_factor = 1.5
    pe_ratio = 1*enricos_correction_factor
    if s/rw > 0.0:
        # enricos_correction_factor = 1.2 #to make things working, big deviatio when you increas too much density
        pe_ratio = np.log(s/rw) / ( np.log( s/(2*rw) + np.sqrt( (s/(2*rw))**2 - 1 )   ) )*enricos_correction_factor
    # here, I decided to assume the current lines are closer, hence there is this factor +0.15
    # this choice is more conservative, and allows to not have an overestimation of the inductance
    # pe_ratio += 0.15
    if pe_ratio > 0 and pe_ratio < 10.0:
        logger.info("proximity effect ratio: {:.2f} for s/rw={:.2f}".format(pe_ratio, s/rw))
        # update inductance
        Ltot = Ltot/pe_ratio
    else:    
        logger.info("proximity effect ratio CANNOT be used here?? : {:.2f} for s/rw={:.2f}".format(pe_ratio, s/rw))

    filling_factor = copper_surface/(a*b)
    logger.info("filling factor {:.2f}".format(filling_factor))

    logger.info("Estimated overall self-inductance: {:.2f} uH".format(Ltot/1e-6))
    out['Ltot'] = Ltot
    out['L_geometry'] = geometry
    out['L_is_geometry_spiral'] = geometry == "spiral"
    if geometry == "spiral":
        logger.info("spiral inductance calculated with loops... and not with wheeler")
    logger.info("Global cumulative surface: {:.2f} m^2".format(Stot))
    out['Stot'] = Stot
    return Ltot


#----------------------------------------------------------------------
def calculate_resistance(p, out=None):
    '''
    calcualte the resistance of the copper tracks

    :param p: coil parameters (dict)
    :param out: (optional) if a dict is passed, it will be completed with the intermediary and more detailed results (i.e. those in the logs; see below for fields).

    Enrico Gasparin

    .. note:: Fields for the optional output in out

       coil
          input coil parameters (copy of p)
       R_rho
          resistivity used for calculations (cu_rho internally, i.e. that of Cu)
       length
          Estimated wire length (m)
       Rtot
          Estimated overall resistance (ohm)

    '''

    if not isinstance(out,dict): out = {} # makes all the other statements smaller but no effect ouside if out is not a dict!
    out['coil'] = p.copy()
    
    cu_rho = 1.68e-8
    out['R_rho'] = cu_rho

    # coil construction
    cu_w = p['cu_w']   #copper width
    cu_d = p['cu_d']   #copper distance
    cu_t = p['cu_t']    #copper thicknes
    layers = p['layers']
    layers_d = p.get('layers_d')
    if layers_d is None:
        layers_d = p.setdefault('layers_d',get_layer_distance(layers)) #p['layers_d'] # distance among layers

    # coil interconnection
    line_w = p['line_w']
    line_d = p['line_d']
    line_length = p['line_length']

    # electric properties
    epsilon_r = p['epsilon_r'] # good approximation for FR4

    # geometry
    n = p['n']  # turns
    geometry = p['geometry']
    a = p['a']
    b = p['b']
    r = p['r'] #outer radius -- for spiral only


    Rtot = 0

    length = 0
    
    if geometry == "rectangular":
        for j in range(n):
            length += 2*(a -j*(cu_d + cu_w)*2) + 2*(b - j*(cu_d + cu_w)*2)
            # print length
    if geometry == "spiral":
        for j in range(n):
            length += 2*np.pi*(r - j*(cu_d + cu_w)*2)

    length = length*layers

    Rtot = cu_rho*length/(cu_w*cu_t)

    logger.info("Estimated wire length {:.2f} m".format(length))
    out['length'] = length
    logger.info("Estimated overall resistance {:.2f} ohms".format(Rtot))
    out['Rtot'] = Rtot

    return Rtot

#----------------------------------------------------------------------
def calculate_coil(p, out=None):
    """Calculates all electromagnetic characteristics of a coil

    Simple aggregation of the calls to :func:`calculate_self_capacitance`,
    :func:`calculate_self_inductance` and :func:`calculate_resistance`.

    :param p: coil parameters (dict)
    :param out: (optional) no need for this as the internally created one is returned as the output of the function; if a dict is passed, it will be completed with the intermediary and more detailed results (i.e. those in the logs; see below for fields).

    :return: dict with different results (the completed out dict if passed)

    .. note::
       
       Side effect: modifies the coil parameters (p) by adding the calculation results and some geometrical parameters:

       calculated
          dict with the calculated results
       D, N, a_i, b_i
          geometrical parameters calculated by :func:`calculate_additional_geometrical_parameters`

    .. note::
       
       Adds the estimated resonance frequency from the computed inductance and capacitance:
       
       f_res
          Resonance frequency (Hz)
       omega_res
          Resonance frequency (pulsation) (rad/s)
       Q
          Estimated quality factor
       xsi
          Estimated damping factor
       wl
          Estimated electric pole
       calculate_coil.message
          Info string about results

    """
    if not isinstance(out,dict): out = {} # makes all the other statements smaller but no effect ouside if out is not a dict!
    calculate_additional_geometrical_parameters(p)
    calc = p['calculated'] = {}
    L = calc['L'] = calculate_self_inductance(p,out)
    C = calc['C'] = calculate_self_capacitance(p,out)
    R = calc['R'] = calculate_resistance(p,out)
    out['coil'] = p.copy()
    omega_res = out['omega_res'] = 1.0/np.sqrt(L*C)
    f_res = out['f_res'] = omega_res / 2.0 / np.pi
    Q = out['Q'] = 1/R*np.sqrt(L/C)
    xsi = out['xsi'] = 0.5*R*np.sqrt(C/L)
    wl = out['wl'] = (R/L)/(2*np.pi)
    D = p['cu_w'] + p['cu_d'] # = p['D'] from calculate_additional_geometrical_parameters
    N = p['n'] * p['layers'] # = p['N'] from calculate_additional_geometrical_parameters
    msg = out['calculate_coil.message'] = '''
coil {}:

       L = {:.2f} (uH)
       C = {:.2f} (pF)
       R = {:.2f} (ohm)

   f_res = {:.3f} (kHz)
       Q = {:.2f}
      xi = {:.3f}
      wl = {:.2f} (kHz)

       a = {:.1f} (mm)
       b = {:.1f} (mm)
       D = {:.1f} (um)
       N = {:d}
       n = {:d}
  layers = {:d}
'''.format(p['sensor'],L/1e-6,C/1e-12,R,f_res/1e3,Q,xsi,wl/1e3,p['a']*1e3,p['b']*1e3,D*1e6,N,p['n'],p['layers'])
    logger.info(msg)
    return out

#----------------------------------------------------------------------
def get_vals_at_specific_freq(frsp, work_freq):
        '''
        helper function, returns some useful value at a specified working frequency.
        Among others, it is relevant the current ratio il/ic, where il is the current circulating in the inductor, and ic in the parasitic capacitance.
        Enrico: a good coil should have a ratio il/ic at least of ~40

        :param frsp: the coil frequency response, as result coming from the function "calculate_coil_freq_resp"
        :param work_freq: the working frequency in Hz
        '''
        idx = (np.abs(frsp['f']- work_freq)).argmin() #find index to ha
        
        il = frsp['ampl_il'][idx] # the inductor and
        ic = frsp['ampl_ic'][idx] # the parasitic capacitance currents
        iratio =  frsp['ampl_il'][idx]/frsp['ampl_ic'][idx]
        
        z = frsp['ampl_z'][idx] #the impendace at the working frequency

        return {'iratio':iratio, 'il':il , 'ic':ic, 'z': z,'f':work_freq}
#----------------------------------------------------------------------
def calculate_coil_freq_resp(R, L, C, Cr=0, w = None):
    '''
    calculate the frequency response of the coil impedance, based on its model

    R, L, C : coil impedance parameter
    Cpar : additional parallel capacitance (used in the case of rx resonant coil)
    w :optional vector of frequency (in rad/s)
    '''

    #  impedance 
    z_d = [(L*C + L*Cr), ( R*C + R*Cr ), 1]
    z_n = [0, L, R]

    # impedances ratio, useful to have an estimate of the current that will flow in the parasitic capacitance C
    ic_d = [(L*C + L*Cr), ( R*C + R*Cr ), 1]
    ic_n =  [L*C, R*C, 0]

    # additional parallel capacitor
    icr_d = [(L*C + L*Cr), ( R*C + R*Cr ), 1]
    icr_n =  [L*Cr, R*Cr, 0]

    # inductor and resistor in series
    il_d = [(L*C + L*Cr), ( R*C + R*Cr ), 1]
    il_n =  [0, 0, 1]

    Zf = signal.lti(z_n, z_d)
    Icf = signal.lti(ic_n, ic_d)
    Icrf = signal.lti(icr_n, icr_d)
    Ilf = signal.lti(il_n, il_d)
    
    if w is None:
        w = np.linspace(1,6e6*2*np.pi,1e5)


    w_z, mag_z, phase_z = signal.bode(Zf, w )
    w_ic, mag_ic, phase_ic = signal.bode(Icf, w )
    w_icr, mag_icr, phase_ic = signal.bode(Icrf, w )
    w_il, mag_il, phase_il = signal.bode(Ilf, w )

    f = w/(2*np.pi)
    ampl_ic = 10**(mag_ic/20.0)
    ampl_icr = 10**(mag_icr/20.0)
    ampl_il = 10**(mag_il/20.0)
    ampl_z = 10**(mag_z/20.0)


    return {'f':f, 'ampl_z':ampl_z, 'ampl_ic':ampl_ic, 'ampl_icr':ampl_icr, 'ampl_il':ampl_il}



#----------------------------------------------------------------------
def calculate_bode_diagrams(R, L, C, Vs = 5.0, Rg = 50.0, sensor_name = '', w = None):
    '''
    calculate the bode diagram of the current circulating in a coil

    R, L, C : coil impedance parameter
    Vs : supply voltage
    Rg: generator series resistance
    w :optional vector of frequency (in rad/s)
    '''

    # plot the bode diagram of the impedance
    # add the generator resistance
    # Rg = 50.0

    # R = 1.0
    # L = 10e-6
    # C = 50e-9
    
    # print L,R,C, (R/L)/6.28
    
    # supply voltage
    # Vs = 5.0

    #  impedance
    # g_d = [(L*C), (C*R), 1]
    # g_n = [(L*C*Rg), ( C*Rg*R +L ), (Rg+R)]
    


    # inverse impedance (admittance)
    z_d = [(L*C*Rg), ( C*Rg*R +L ), (Rg+R)]
    z_n = [Vs*(L*C), Vs*(C*R), Vs*1]
    
    # normalised inductor current
    i_d = [(L*C*Rg), ( C*Rg*R +L ), (Rg+R)]
    i_n = [Vs*1]

    # normalised parasitic capacitor current
    c_d = [(L*C*Rg), ( C*Rg*R +L ), (Rg+R)]
    c_n = [Vs*L*C, R*C, 0]

    If = signal.lti(i_n, i_d)
    Zf = signal.lti(z_n, z_d)
    Gf = signal.lti(g_n, g_d)
    Cf = signal.lti(c_n, c_d)
    if w is None:
        w = np.linspace(1,6e6*2*np.pi,1e5)
    
    w_z, mag_z, phase_z = signal.bode(Zf, w )
    w_g, mag_g, phase_g = signal.bode(Gf, w )
    w_i, mag_i, phase_i = signal.bode(If, w )
    w_c, mag_c, phase_c = signal.bode(Cf, w )

    f = w/(2*np.pi)
    ampl_g = 10**(mag_g/20.0)
    ampl_z = 10**(mag_z/20.0)
    ampl_i = 10**(mag_i/20.0)
    ampl_c = 10**(mag_c/20.0)



    # to be moved out, there is no sense having a plot in this function
    plt.figure(100)
    plt.semilogx(f, ampl_z, label='total current')    # Bode magnitude plot
    plt.semilogx(f, ampl_i, label='coil current')
    plt.semilogx(f, ampl_c, label='cap current')
    plt.grid()
    plt.legend()
    plt.title( sensor_name + " current @ {:.2f}V, Rg = {:.2f}".format(Vs, Rg) )


    return {'f':f, 'ampl_z':ampl_z, 'ampl_i':ampl_i, 'ampl_c':ampl_c}

#======================================================================

#----------------------------------------------------------------------
def calculate_additional_geometrical_parameters(p, out=None):
    """Computes several geometrical parameters

    These are stored in the coil structure dict (p)

    :param p: coil parameters (dict)
    :param out: (optional) no need for this as the internally created one is returned as the output of the function; if a dict is passed, it will be completed with the intermediary and more detailed results (i.e. those in the logs; see below for fields).

    .. note::
       
       Adds the following fields to the coil parameters:

       D
          distance from track centre to track centre
       N
          total number of turns
       a_i
          list of dimensions of rectangular inner coils (a)
       b_i
          list of dimensions of rectangular inner coils (b)

    """
    if not isinstance(out,dict): out = {} # makes all the other statements smaller but no effect ouside if out is not a dict!
    out['coil'] = p.copy()
    a,b,n = p['a'],p['b'],p['n']
    D = p['D'] = p['cu_d'] + p['cu_w']
    N = p['N'] = n * p['layers']
    j = np.arange(1,n+1)
    p['a_i'] = (a - D * (2*j-1)).tolist()
    p['b_i'] = (b - 2*D * (j-1)).tolist()

def calc_D(amax, amin, n, mode='a'):
    """Helper func returning the distance between track centres (D)

    From the max and the min (and the number of turns per layer).

    ...

    """
    f = 2*n-1 if mode == 'a' else 2*(n-1)
    return (amax-amin)/float(f)

if __name__ == "__main__":

    # logger setup
    logger = logging.getLogger()
    hdlr = RotatingFileHandler(
        "./_logs/coils_calc.log", mode="a", maxBytes=5 * 1024 * 1024, backupCount=2, encoding=None, delay=0
    )
    logFormatter = logging.Formatter("%(asctime)s :: %(levelname)s :: %(message)s")
    hdlr.setFormatter(logFormatter)
    logger.addHandler(hdlr)
    # set logger level
    logger.setLevel(logging.DEBUG)
    logger.setLevel(logging.INFO)

    logToConsole = True
    # additional setup, to log to console too - more for debug or terminal operations
    if logToConsole:
        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(logFormatter)
        logger.addHandler(consoleHandler)


    filename = dir_path + "/" + sys.argv[1] #'VB4_square.json'

    with open(filename) as json_file:
        coil = json.load(json_file)

    name = os.path.splitext(filename)[0]
    

    
    logger.info("################### ---- SENSOR: %s ---- ###################", coil['sensor'])

    logger.info("x dimension: {:.2f} mm".format(coil['a']*1.0e3))

    logger.info("y dimension : {:.2f} mm".format(coil['b']*1.0e3))

    logger.info("number of layers : {:d}".format(coil['layers']))

    logger.info("number of turns : {:d}".format(coil['n']))

    logger.info("track width : {:.2f} um".format(coil['cu_w']*1e6))

    logger.info("track clearance : {:.2f} um".format(coil['cu_d']*1e6))


    logger.info("---- SELF INDUCTANCE ----")

    L = calculate_self_inductance(coil)
    logger.info("---- RESISTANCE ----")

    R = calculate_resistance(coil)
    

    logger.info("---- SELF CAPACITANCE ----")

    C1 = calculate_self_capacitance(coil, shielding=False)
    C2 = calculate_self_capacitance(coil, shielding=True)


    labels = ['','(shielding)']
    jj = 0
    for C in [C1, C2]:

        logger.info("---- INFO ---- {:s}".format(labels[jj]))

        fn = (1/np.sqrt(C*L))/(2*np.pi)
        logger.info("Estimated self-resonance: {:.2f} kHz".format(fn/1e3))

        Q = 1/R*np.sqrt(L/C)
        logger.info("Estimated quality factor: {:.2f}".format(Q))

        xsi = 0.5*R*np.sqrt(C/L)
        logger.info("Estimated damping factor: {:.2f}".format(xsi))

        wl = (R/L)/(2*np.pi)
        logger.info("Estimated electric pole: {:.2f} kHz".format(wl/1.e3))

    
        logger.info("---- DEVIATION TO REAL MEASUREMENTS ----")

        exp_L = coil['exp_L'] # load some experimental values (if available)
        exp_C = coil['exp_C'] # load some experimental values (if available)
        exp_R = coil['exp_R'] # load some experimental values (if available)
        # exp_F = coil['exp_F']

        if exp_L > 0:
            errL = L - exp_L
            logger.info("Experimental inductance {:.2f} uH -> abs error: {:.2f} uH ({:.2f}%)".format(exp_L*1e6, errL*1e6, errL/exp_L*100))

        if exp_C > 0:
            errC = C - exp_C
            logger.info("Experimental capacitance {:.2f} pF -> abs error: {:.2f} pf ({:.2f}%)".format(exp_C*1e12, errC*1e12, errC/exp_C*100))


        if exp_R > 0:
            errR = R - exp_R
            logger.info("Experimental resistance {:.2f} ohm -> abs error: {:.2f} ohm ({:.2f}%)".format(exp_R, errR, errR/exp_R*100))


        if exp_L > 0 and exp_C > 0:
            wexp = 1/np.sqrt(exp_L*exp_C)
            logger.info("Experimental resonance frequency {:.2f} kHz".format(1e-3*wexp/(2*np.pi)))
    



        pancake_coils_draw.draw_coil(coil,name)

        # calculate_bode_diagrams(R,L,C,sensor_name=coil['sensor'])


        work_freq = 130e3
        str_plt = ''
        # plot a bode diagram of the coil
        frsp = calculate_coil_freq_resp(R, L, C)



        fnum = 100 + jj*1000
        plt.figure(fnum)
        idx = (np.abs(frsp['f']- work_freq)).argmin() #find index to ha
        plt.semilogx(frsp['f'], frsp['ampl_z'], label='coil impedance {:.2f}ohm@{:.2f}kHz'.format(frsp['ampl_z'][idx],work_freq*1e-3))    # Bode magnitude plot
        plt.plot(frsp['f'][idx], frsp['ampl_z'][idx],'*r',markersize=5)
        plt.grid()
        plt.legend()
        plt.title('Final result {:s} @ {:.2f}kHz '.format(coil['sensor'], work_freq*1e-3) + '\n' + str_plt, fontsize=10)
        # plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

        fnum = 101 + jj*1000
        plt.figure(fnum)
        idx = (np.abs(frsp['f']- work_freq)).argmin() #find index to ha
        plt.semilogx(frsp['f'], frsp['ampl_ic'], label='Ic : parasitic capacitor current ratio')   # Bode magnitude plot
        plt.plot(frsp['f'][idx], frsp['ampl_ic'][idx],'*r',markersize=5)
        plt.semilogx(frsp['f'], frsp['ampl_icr'], label='Icr : resonant capacitor current ratio')   # Bode magnitude plot
        plt.plot(frsp['f'][idx], frsp['ampl_icr'][idx],'*r',markersize=5)
        plt.semilogx(frsp['f'], frsp['ampl_il'], label='Il : inductor-resistor current ratio')   # Bode magnitude plot

        # coil/parasitic capitance currents ratio at the working frequency
        iratio =  frsp['ampl_il'][idx]/frsp['ampl_ic'][idx]

        plt.plot(frsp['f'][idx], frsp['ampl_il'][idx],'*r',markersize=5)
        plt.text(1, 1.2,"curr. ratio Il/Ic = {:.2f}@{:.2f}kHz".format(iratio,work_freq*1e-3))
        plt.ylim([0, 6])
        plt.grid()
        plt.legend()
        plt.title('Final result {:s} @ {:.2f}kHz '.format(coil['sensor'], work_freq*1e-3) + '\n' + str_plt, fontsize=10)
        # plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')



        jj += 1

    plt.show()
    



