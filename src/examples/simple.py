# -*- coding: utf-8 -*-
"""Simple example (for experimenting with ipython)
===============================================

...

With ipython:

>>> cd C:\Users\gille\Documents\codes\sensor_design\src
>>> activate base2
>>> ipython --gui=qt --matplotlib=qt

then, cut and paste parts.

or run it with python:

>>> cd C:\Users\gille\Documents\codes\sensor_design\src\examples
>>> activate base2
>>> python simple.py

"""
import os
import sys
try:
    script_path = os.path.dirname(os.path.realpath(__file__))
    src_path = os.path.join(script_path,'..')
except:
    src_path = 'C:/Users/gille/Documents/codes/sensor_design/src'

sys.path.append(src_path)

import json
from pancake_coils_calc import calculate_coil
from pancake_coils_draw import draw_coil, generate_dxf
import matplotlib.pyplot as plt

coils_folder = os.path.join(src_path,'coils_geom')

coil_name = 'VB47T_A'
coil_name = 'minitest'
coil_name = 'VB46R_D'
coil_name = 'VB46R_E'
coil_name = 'VB46R_F'
coil_name = 'RS_TX_0004S1'
coil_name = 'RS_TX_0004S1_RS_VB48'
coil_name = 'VB45_A_rect'
coil_name = 'VB45_A_rect_rerun'
coil_name = 'VB48R_H'
coil_name = 'MD_8X_8Y_TX'
coil_name = 'MD_8X_8Y_TX_V2'
coil_name = 'MD_7X_8Y_HOR'
# coil_name = 'MD_8X_8Y_TX_V2_rerun'
coil_name = 'MD8X8Y_TXRX_2LAYERS_V3'
coil_name = 'VB46R_E'

# Loading coil design from JSON
filename = os.path.join(coils_folder,coil_name+'.json')
with open(filename,'rb') as fh: c = json.load(fh)

# Computing stuff
out = calculate_coil(c)
for name, val in c['calculated'].iteritems():
    c[name] = val
print(json.dumps(out,indent=4))
print(out['calculate_coil.message'])

xvect, yvect, linewidth = draw_coil(c,figure=plt.figure())
dxf_doc = generate_dxf(xvect, yvect, linewidth, os.path.join(coils_folder, coil_name + '.dxf'))

with open(os.path.join(coils_folder, coil_name + '_out.json'),'w') as fh:
    json.dump(out, fh, indent=4)

# Plotting
from pancake_coils_draw import draw_coil
import matplotlib.pyplot as plt

# coil_name = 'minitest'
# filename = os.path.join(coils_folder,coil_name+'.json')
# with open(filename,'rb') as fh: c = json.load(fh)
# draw_coil(c,figure=plt.figure())
# plt.ylim(-0.1*c['a'],1.1*c['a'])
# plt.xlim(-0.1*c['b'],1.1*c['b'])
plt.show()
