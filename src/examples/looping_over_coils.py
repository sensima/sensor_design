# -*- coding: utf-8 -*-
"""Example looping over several coil designs
=========================================

...

With ipython:
>>> cd C:\Users\gille\OneDrive\Documents\codes\sensor_design\src
>>> activate base2
>>> ipython --gui=qt --matplotlib=qt

then, cut and paste parts.

or run it with python:
>>> cd C:\Users\gille\OneDrive\Documents\codes\sensor_design\src\examples
>>> activate base2
>>> python looping_over_coils.py

"""
import os
import sys
try:
    script_path = os.path.dirname(os.path.realpath(__file__))
    src_path = os.path.join(script_path,'..')
except:
    src_path = 'C:/Users/gille/Documents/codes/sensor_design/src'

sys.path.append(src_path)

import json
from pancake_coils_calc import calculate_coil
from pancake_coils_draw import draw_coil
from misc import load_coil, save_coil
import matplotlib.pyplot as plt

coils_folder = os.path.join(src_path,'coils_geom')

coil_names = [
    'VB47T_A',
    'minitest',
    'VB46R_D',
    'VB46R_E',
    'VB46R_F',
]

results = {}
for cn in coil_names:
    c = load_coil(cn+'.json')
    results[cn] = calculate_coil(c)

for cn, out in results.iteritems():
    print '{}\n\n'.format(out['calculate_coil.message'])
    
for cn, out in results.iteritems():
    print json.dumps(out,indent=4)


# Plotting

plt.close('all')
for cn, out in results.iteritems():
    draw_coil(out['coil'],figure=plt.figure())

plt.show()
