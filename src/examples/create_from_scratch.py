# -*- coding: utf-8 -*-
"""Example for creating a coil from scratch
=========================================

...

With ipython:
>>> cd C:\Users\gille\OneDrive\Documents\codes\sensor_design\src
>>> activate base2
>>> ipython --gui=qt --matplotlib=qt

then, cut and paste parts.

or run it with python:
>>> cd C:\Users\gille\OneDrive\Documents\codes\sensor_design\src\examples
>>> activate base2
>>> python create_from_scratch.py

"""
import os
import sys
try:
    script_path = os.path.dirname(os.path.realpath(__file__))
    src_path = os.path.join(script_path,'..')
except:
    src_path = 'C:/Users/gille/Documents/codes/sensor_design/src'

sys.path.append(src_path)

import json
from pancake_coils_calc import calculate_coil
from pancake_coils_draw import draw_coil
import matplotlib.pyplot as plt

coils_folder = os.path.join(src_path,'coils_geom')

c0 = {
    "_notes": "pancake coil used to build up the integrated concept VB46 - coil 2 (middle)",
    "sensor": "VB46R_E",
    "cu_w": 200e-6, 
    "cu_d": 3500e-6,
    "cu_t": 35e-6,
    "layers": 2,
    "layers_d": 1.2e-3,
    "layers_skewing":False,
    "line_w" : 250e-6,
    "line_d" : 250e-6,
    "line_length" : 5e-2,
    "epsilon_r" : 4.7,
    "geometry" : "rectangular",
    "n" : 16,
    "a" : 120e-3,
    "b" : 380e-3,
    "r" : 0,
    "exp_L":240e-6,
    "exp_R":0,
    "exp_C":0,
}

coil = c0.copy()
coil.update(
    {
        "_notes": "pancake coil used to build up the integrated concept VB48 - coil 2 (middle)",
        "sensor": "VB48R_E",
        "cu_d": 3315e-6,
    }
)

results = calculate_coil(coil)
print '{}\n\n'.format(results['calculate_coil.message'])

draw_coil(results['coil'],figure=plt.figure())

plt.show()
