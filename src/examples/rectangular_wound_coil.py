# -*- coding: utf-8 -*-
"""Example for creating a rectangular wound coil from scratch
=========================================

...

With ipython:
>>> cd C:\Users\gille\OneDrive\Documents\codes\sensor_design\src
>>> activate base2
>>> ipython --gui=qt --matplotlib=qt

then, cut and paste parts.

or run it with python:
>>> cd C:\Users\gille\OneDrive\Documents\codes\sensor_design\src\examples
>>> activate base2
>>> python create_from_scratch.py

"""
import os
import sys
try:
    script_path = os.path.dirname(os.path.realpath(__file__))
    src_path = os.path.join(script_path,'..')
except Exception as e:
    print 'Exception (ignored): {}'.format(e)
    src_path = 'C:/Users/gille/Documents/codes/sensor_design/src'

sys.path.append(src_path)

import json
from pancake_coils_calc import calculate_coil
from pancake_coils_draw import draw_coil
import matplotlib.pyplot as plt

coils_folder = os.path.join(src_path,'coils_geom')

# Coil formers definitions (from BY drawing)
tx_coil_former = dict(
    a_outer = 120e-3,
    b_outer = 150e-3,
    a_inner = 100e-3, # 10 mm clearance
    b_inner = 130e-3, # 10 mm clearance
)
print('Tx coil former:\n{}'.format(json.dumps(tx_coil_former,indent=4)))

rx_coil_former = dict(
    a_outer = 60e-3,
    b_outer = 150e-3,
    a_inner = 30e-3, # 20,10 mm clearance
    b_inner = 110e-3, # 20 mm clearance
)
print('Rx coil former:\n{}'.format(json.dumps(rx_coil_former,indent=4)))

# Some coil definitions
a_in = tx_coil_former['a_inner']
b_in = tx_coil_former['b_inner']
wire_cu_diameter = 350e-6
wire_diameter = wire_cu_diameter + 50e-6
n_layers = 11 # 8
n = 6 # 4
a0 = a_in + wire_cu_diameter
a = a0 + (n-1)*wire_diameter + wire_cu_diameter
b0 = b_in + wire_cu_diameter
b = b0 + (n-1)*wire_diameter + wire_cu_diameter

# coil dict...
c0 = {
    "_notes": "fake pancake coil to simulate a rectangular wound coil",
    "sensor": "Tx-special",
    "cu_w": wire_cu_diameter, 
    "cu_d": wire_diameter-wire_cu_diameter,
    "cu_t": wire_cu_diameter,
    "layers": n_layers,
    "layers_d": wire_diameter,
    "layers_skewing":False,
    "line_w" : 2500e-6,
    "line_d" : 2500e-6,
    "line_length" : 5e-2,
    "epsilon_r" : 4.7,
    "geometry" : "rectangular",
    "n" : n,
    "a" : a,
    "b" : b,
    "r" : 0,
    "exp_L":240e-6,
    "exp_R":0,
    "exp_C":0,
    "field_definitions": {
	"description": "this is the wound coil used for the MS1B of RS project (QPS integration June 2020) ",
	"sensor": "the UNIQUE name that allows to identify the sensor. ex: can be the same as the filename.",
	"cu_w": "width of the copper tracks",
	"cu_d": "clearence among the copper tracks",
	"cu_t": "thickness of the copper tracks",
	"layers": "number of layers", 
	"layers_d": "clearence among the layers on the stack",
	"line_w": "track width of the line used to route the coil to the connector (can be a rough estimation) ",
	"line_d": "clearence among the lines used to route the coil to the connector (can be a rough estimation)",
	"line_length": "length of the lines used to route the coil to the connector (can be a rough estimation)",
	"epsilon_r": "dielectric constant of the FR4/or PCB bulk material",
	"geometry": "choice of the coil geometry, you can selecte 'spiral' or 'rectangular' ",
	"n": "number of turns of the coil",
	"a": "only for rectangular geometry: length of the side 1 of the rectangle",
	"b": "only for rectangular geometry: length of the side 2 of the rectangle",
	"r": "only for spiral geometry: outer radius",
	"exp_L": "experimental measurement of the inductance. put 0 if not available.",
	"exp_R": "experimental measurement of the resistance. put 0 if not available.",
	"exp_C": "experimental measurement of the self-capacitance. put 0 if not available.",
    },
}

coil = c0.copy()
coil.update(
    dict(
        layers = 11, # 8
        n = 6, # 4
    )
)

results = calculate_coil(coil)
print '{}\n\n'.format(results['calculate_coil.message'])

draw_coil(results['coil'],figure=plt.figure())

plt.show()
