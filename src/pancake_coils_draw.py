# -*- coding: utf-8 -*-
"""

useful functions to plot graphically the designed coil

__author__      = "Enrico Gasparin"

2020-03-04


"""



import json
import matplotlib.pyplot as plt

import os, sys
dir_path = os.path.dirname(os.path.realpath(__file__))


import numpy as np


class data_linewidth_plot():
    '''
    noite very proud, but example taken from 
    https://stackoverflow.com/questions/19394505/matplotlib-expand-the-line-with-specified-width-in-data-unit

    
    '''
    def __init__(self, x, y, **kwargs):
        self.ax = kwargs.pop("ax", plt.gca())
        self.fig = self.ax.get_figure()
        self.lw_data = kwargs.pop("linewidth", 1)
        self.lw = 1
        self.fig.canvas.draw()

        self.ppd = 72./self.fig.dpi
        self.trans = self.ax.transData.transform
        self.linehandle, = self.ax.plot([],[],**kwargs)
        if "label" in kwargs: kwargs.pop("label")
        self.line, = self.ax.plot(x, y, **kwargs)
        self.line.set_color(self.linehandle.get_color())
        self._resize()
        self.cid = self.fig.canvas.mpl_connect('draw_event', self._resize)

    def _resize(self, event=None):
        lw =  ((self.trans((1, self.lw_data))-self.trans((0, 0)))*self.ppd)[1]
        if lw != self.lw:
            self.line.set_linewidth(lw)
            self.lw = lw
            self._redraw_later()

    def _redraw_later(self):
        self.timer = self.fig.canvas.new_timer(interval=10)
        self.timer.single_shot = True
        self.timer.add_callback(lambda : self.fig.canvas.draw_idle())
        self.timer.start()


    

def draw_coil(coil, name=None, units=(1e-3,'mm'), figure=None):
    """Draws the coil (top view)

    :param coil: coil to draw

    :param name: If not None, the plot is saved (PNG) with this name

    :param units: Used to convert dimensions before plotting (If None: SI)

    :param figure: Optional figure to plot in

    """

    x_scale_factor,unit_name = units if units is not None else (1.0,'m')
    
    # coil construction
    cu_w = coil['cu_w']   #copper width
    cu_d = coil['cu_d']   #copper distance
    cu_t = coil['cu_t']    #copper thicknes
    layers = coil['layers']
    layers_d = coil['layers_d'] # distance among layers

    # coil interconnection
    line_w = coil['line_w']
    line_d = coil['line_d']
    line_length = coil['line_length']

    # electric properties
    epsilon_r = coil['epsilon_r'] # good approximation for FR4

    # geometry
    n = int(coil['n'])  # turns
    geometry = coil['geometry']
    a = coil['a']
    b = coil['b']
    r = coil['r'] #outer radius -- for spiral only


    if figure is None:
        fig = plt.figure(1)
    else:
        fig = plt.figure(figure.number)


    if geometry == "rectangular":
        
        x0 = 0
        y0 = 0

        xvect = [x0]
        yvect = [y0]

        dw = cu_d + cu_w

        for j in range(int(n)):
            
            x0 = 0 + j*dw 
            y0 = a - j*dw 
            xvect.append(x0/x_scale_factor)
            yvect.append(y0/x_scale_factor)

            x0 = b - j*dw 
            y0 = a - j*dw 
            xvect.append(x0/x_scale_factor)
            yvect.append(y0/x_scale_factor)

            x0 = b - j*dw 
            y0 = (1+j)*dw
            xvect.append(x0/x_scale_factor)
            yvect.append(y0/x_scale_factor)

            x0 = (1+j)*dw
            y0 = (1+j)*dw
            xvect.append(x0/x_scale_factor)
            yvect.append(y0/x_scale_factor)


        # plt.plot(xvect,yvect,linewidth = 2.0)
        l = data_linewidth_plot(xvect, yvect, linewidth=coil['cu_w']/x_scale_factor, color='k')


        ax = plt.gca()
        ax.set_aspect('equal')



    elif geometry == "spiral":
        
        theta = np.linspace(0,2*np.pi*n, n*360) #360 points per turn
        A = r + theta/(2*np.pi)*dw
        xvect = A*np.cos(theta)/x_scale_factor
        yvect = A*np.sin(theta)/x_scale_factor

        # plt.plot(xvect,yvect,linewidth = 2.0)
        data_linewidth_plot(xvect, yvect, linewidth=coil['cu_w']/x_scale_factor, color='k')
        ax = plt.gca()
        ax.set_aspect('equal')
        pass

    else:

        raise ValueError('Unrecognized geometry: {}'/format(geometry))


    plt.title(coil['sensor'])
    plt.grid()

    plt.xlabel('x ({})'.format(unit_name))
    plt.ylabel('y ({})'.format(unit_name))

    # plt.figure(1)
    if name is not None:
        plt.savefig(name+'.png')
    
    linewidth = coil['cu_w']/x_scale_factor


    return (xvect, yvect, linewidth)


def generate_dxf(xvect, yvect=None, linewidth=150e-6, save_filename=None):
    """Generate a dxf document with the given vertices and linewidth (e.g.
    the ones given by the draw_coil function). If save_filename is given,
    also saves the document to disk.

    :param xvect: x-coordinates of vertices or, if yvect is None, numpy
       array with segments' information (shape: (n_segments, 2, 3))

    :param yvect: y-coordinates of vertices

    :param linewidth: track width (default: 150 um)

    :param save_filename: full filename used to save the generated dxf (optional,
        no save action performed if not given)

    :return: the generated dxf document

    """
    import ezdxf

    doc = ezdxf.new('R2000')
    msp = doc.modelspace()

    if yvect is None:
        for i,x in enumerate(xvect):
            msp.add_lwpolyline(
                [x[0,:2], x[1,:2]],
                dxfattribs = {
                    'const_width': linewidth,
                    'layer': 'z=' + str(x[0,2])
                },
            )
    else:
        for i in range(len(xvect) - 1):
            msp.add_lwpolyline([(xvect[i], yvect[i]), (xvect[i+1], yvect[i+1])], dxfattribs={'const_width': linewidth})
    
    if save_filename is not None:
        doc.saveas(save_filename)

    return doc

if __name__ == "__main__":


    filename = sys.argv[1] #'VB4_square.json'

    with open(filename) as json_file:
        coil = json.load(json_file)

    draw_coil(coil)
    plt.show()
