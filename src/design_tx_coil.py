# -*- coding: utf-8 -*-
"""

Collection of useful scripts, fucntions, methods for the design of air wounded coils.

__author__      = "Enrico Gasparin"

2020-06-30

arguments
ARG1: excitation frequency in kHz 
ARG2: excitation voltage in Volts
ARG3: maximum allowed excitation current in mA
ARG4: generator series resistor in Ohm
ARG5: number of layers
ARG6: targeted coils self-resonant frequency in kHz 

python .\design_tx_coil.py coils_geom\VB46T_C.json 130 5 85 0

"""
from logging.handlers import RotatingFileHandler
import logging
# setup rotating file logger
logger = logging.getLogger()
hdlr = RotatingFileHandler(
    "./_logs/coils_calc.log", mode="a", maxBytes=5 * 1024 * 1024, backupCount=2, encoding=None, delay=0
)
logFormatter = logging.Formatter("%(asctime)s :: %(levelname)s :: %(message)s")
hdlr.setFormatter(logFormatter)
logger.addHandler(hdlr)
# set logger level
# logger.setLevel(logging.DEBUG)
# logger.setLevel(logging.INFO)
logger.setLevel(logging.ERROR)

logToConsole = True
# additional setup, to log to console too - more for debug or terminal operations
if logToConsole:
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    logger.addHandler(consoleHandler)

import numpy as np
import scipy.constants as cnst
from scipy import signal


import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

import json
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
import sys

import pancake_coils_draw as pcd

import pancake_coils_calc as pcc

import passive_components

# I do suppress warnings....
import warnings
warnings.filterwarnings("ignore")

if __name__ == "__main__":

    # base design for the dimensioning
    filename = dir_path + "/" + sys.argv[1] #'VB4_square.json'

    with open(filename) as json_file:
        coil = json.load(json_file)

    # working frequency in kHz
    freq_khz = float(sys.argv[2])
    freq = freq_khz*1e3

    # supply voltage in volts
    max_v = float(sys.argv[3])

    # maximum current in mA
    max_i_ma = float(sys.argv[4])
    max_i = max_i_ma*1e-3

    # series resistance
    # Rg = float(sys.argv[5])

    # coil number of layers
    layers = int(sys.argv[5])



    # self-resonance frequency in kHz
    selfres_freq_khz = float(sys.argv[6])
    selfres_freq = selfres_freq_khz*1e3

    # I would like to use resonance?
    is_resonant_exc = True

    aimed_iratio  = int(sys.argv[7])

    # try:
    #     aimed_iratio  = int(sys.argv[8])
    # except:
    #     aimed_iratio = 00.0
    #     print("---> assuming aimed iratio to its default value, aimed_iratio={:.2f}".format(aimed_iratio))

    # get out some relevant parameter
    a = coil['a']
    b = coil['b']
    min_track_width = coil['cu_w']
    # cu_d = coil['cu_d']

    

    # given the dimensions, I do iterate on the possible combinations
    nn = 20 #10 #30
    cu_w_list = np.linspace(min_track_width,1000e-6,nn) 
    cu_d_list = np.linspace(min_track_width,10000e-6,nn) 
    a0_list = np.linspace(coil['a0'],a,nn) 
    layers_list = np.array(range(layers))+1

    def get_zero_matrix():
        return np.zeros((cu_w_list.size,cu_d_list.size,a0_list.size,layers_list.size))

    def get_zero_grid():
        return np.zeros((cu_w_list.size,cu_d_list.size))

    fres_grid = get_zero_matrix() # resonant frequency
    frl_grid = get_zero_matrix() # electric RL pole
    l_grid = get_zero_matrix()# inductance
    r_grid = get_zero_matrix() # resistance
    rg_grid = get_zero_matrix() # series external resistance
    c_grid = get_zero_matrix() # capacitance selected for the resonance excitation
    q_grid = get_zero_matrix() # quality factor
    i_grid = get_zero_matrix() # current
    phi_grid = get_zero_matrix() # flux
    n_grid = get_zero_matrix() # number of turns
    zeros_grid = get_zero_matrix()
    iratio_grid = get_zero_matrix() # current ration among inductor and self capacitance


    # additional matrices to keep track of the parameters
    cu_w_grid = get_zero_matrix()
    cu_d_grid = get_zero_matrix()
    a0_grid = get_zero_matrix()
    layers_grid = get_zero_matrix()

    # calculate all possible options 
    maxiter = float(cu_w_list.size * cu_d_list.size * a0_list.size * layers_list.size)
    iter = 0

    for i2, layer in enumerate(layers_list):
        for i1, a0 in enumerate(a0_list):
            for j, cu_w in enumerate(cu_w_list):
                for k, cu_d in enumerate(cu_d_list):

                    # fill in the matrices that keep record of the parameters
                    cu_w_grid[j][k][i1][i2]     = cu_w
                    cu_d_grid[j][k][i1][i2]     = cu_d
                    a0_grid[j][k][i1][i2]       = a0
                    layers_grid[j][k][i1][i2]   = layer

                    logger.info("**** iter {:d}".format(iter))

                    # with constraint on the hollow center
                    coil['a0'] = a0
                    b0 = 0.0 # leave b0 as not affecting, one parameter is sufficient..
                    nmax = int(np.floor(np.min([0.5*(a - a0),0.5*(b - b0)])/(cu_w + cu_d))) -1
                    nmax = np.max((nmax,1)) # make sure we guarantee at least 1 turn, whatever you decide


                    logger.info("max number of turns {:.2f}".format(nmax))
                
                    coil['n'] = nmax  # set maximum number of turns
                    coil['cu_w'] = cu_w # set copper width
                    coil['cu_d'] = cu_d # set copper clearancec
                    coil['layers'] = layer

                    # # I do anticipate the layers distance, I take as reference tha layer stackup of EUROCIRCUITS (common..)
                    # if layer == 1 or layer == 2:
                    #     layers_d = 1.55e-3
                    # elif layer == 3 or layer == 4:
                    #     layers_d = 0.6e-3
                    # elif layer == 5 or layer == 6:
                    #     layers_d = 0.3e-3
                    # elif layer == 7 or layer == 8:
                    #     layers_d = 0.2e-3
                    # else:
                    #     raise Exception("number of layers is not supported") 

                    # coil['layers_d'] = layers_d

                    L = pcc.calculate_self_inductance(coil)
                    C = pcc.calculate_self_capacitance(coil, shielding=True)
                    R = pcc.calculate_resistance(coil)

                    # calculate series generator resistance on the base of the dc value
                    Rg = 0.0
                    if (max_v/R > max_i):
                        Rg = max_v/max_i - R

                

                    iter+=1

                    f_res = 1/(2*np.pi*np.sqrt(L*C))
                    logger.info("-> f_res {:.2f} kHz".format(f_res*1e-3))
                    f_rl = (R/L)/(2*np.pi)
                    logger.info("-> f_rl {:.2f} kHz".format(f_rl*1e-3))


                    # take the chance to calculate the current ratio among the inductor and the parallel capacitor
                    frsp = pcc.calculate_coil_freq_resp(R, L, C, w=freq*2*np.pi)
                    iratio_grid[j][k][i1][i2] = pcc.get_vals_at_specific_freq(frsp,freq)['iratio']

                    fres_grid[j][k][i1][i2] = f_res
                    frl_grid[j][k][i1][i2] = f_rl
                    l_grid[j][k][i1][i2] = L
                    r_grid[j][k][i1][i2] = R
                    rg_grid[j][k][i1][i2] = Rg 
                    n_grid[j][k][i1][i2] = nmax
                    q_grid[j][k][i1][i2] = 1/(R+Rg)*np.sqrt(L/C)

                    # frsp = pcc.calculate_bode_diagrams(R, L, C, Vs = max_v, Rg = Rg, w = freq*2*np.pi)


                    

                    if is_resonant_exc:
                        c_est = 1/(L*(2*np.pi*freq)**2)
                        C = passive_components.capacitors.find_nearest_value(c_est)

                        c_grid[j][k][i1][i2] = C
                        i_grid[j][k][i1][i2] = np.abs(max_v/(R + Rg + 1j*L*freq*2*np.pi + 1/(1j*C*freq*2*np.pi) ))
                    else:
                        i_grid[j][k][i1][i2] = np.abs(max_v/(R + Rg + 1j*L*freq*2*np.pi))

                    phi_grid[j][k][i1][i2] = L*i_grid[j][k][i1][i2]

                    

                    #print('...progress {:.2f}%'.format(iter/maxiter*100.) )
                    sys.stdout.write('\r...progress {:.2f}%% - l {:d}; a0 {:.2f}mm; cu_w {:.2f}mm; cu_d {:.2f}mm; '.format(iter/maxiter*100., layer, a0*1e3, cu_w*1e3, cu_d*1e3))
                    sys.stdout.flush()


    # make a selection of the best options
    # 

    # self resonance target
    fres_th = selfres_freq#10*freq #15* threshold for the self-resonance (at least 10 times the excitation freq)
    sel_fres = fres_grid>fres_th #this is the index selection
    # current target - assume a 10% window
    sel_i = (i_grid <= max_i) 
    # print(max_i)


    # selection of the current ratios satisfying the constraint
    sel_iratio = iratio_grid >= aimed_iratio

    # global mask
    final_mask = sel_fres & sel_i & sel_iratio

    if not np.any(final_mask) :
        raise Exception("No solution found") 


    # pick up, as a proposition, the combination within the selection that maximises the flux
    selected_phi = get_zero_matrix()
    selected_phi[final_mask] = phi_grid[final_mask]
    opt_sel = np.unravel_index(np.argmax(selected_phi, axis=None), selected_phi.shape) # for mulit-dimensional case
    # opt_sel = np.where(selected_phi == np.amax(selected_phi))
    # opt_x = opt_sel[1]
    # opt_y = opt_sel[0]


    if (not np.any(final_mask)) or (np.all(selected_phi==0)) :
        raise Exception("No solution found") 


    # also overwrite these parameters to the "coil" structure for future use in this script
    coil["cu_w"] = cu_w_grid[opt_sel] 
    coil["cu_d"] = cu_d_grid[opt_sel]
    coil["a0"] = a0_grid[opt_sel]
    coil["layers"] = layers_grid[opt_sel]
    coil["n"] = n_grid[opt_sel]




    # color map for the mask
    cmap_mask = plt.cm.get_cmap("seismic")


    # plotting
    note = ''
    
    note += "L{:d}T{:d}".format(layers, int(coil["cu_t"]*1e6))



    fnum = 1001
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    plt.title('Self-resonance frequency - {:s}'.format(coil['sensor']))
    X, Y = np.meshgrid(cu_w_list*1e3, cu_d_list*1e3)
    ax.plot_surface(X, Y, fres_grid[:,:,opt_sel[2],opt_sel[3]].T*1e-6, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    ax.scatter(X, Y, get_zero_grid(), c = np.reshape(sel_fres[:,:,opt_sel[2],opt_sel[3]].T, sel_fres[:,:,opt_sel[2],opt_sel[3]].size), cmap= cmap_mask, edgecolor='none', alpha=0.25)
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('fres [MHz]')
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    fnum = 1002
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    plt.title('Attenuation frequency - {:s}'.format(coil['sensor']))
    ax.plot_surface(X, Y, frl_grid[:,:,opt_sel[2],opt_sel[3]].T*1e-3, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('frl [kHz]')
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    fnum = 1003
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, l_grid[:,:,opt_sel[2],opt_sel[3]].T*1e6, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('L [uH]')
    plt.title('Inductance - {:s}'.format(coil['sensor']))
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    # ax.set_zlim(0, 100)
    fnum = 1004
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, r_grid[:,:,opt_sel[2],opt_sel[3]].T, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('R [ohm]')
    plt.title('Resistance - {:s}'.format(coil['sensor']))
    ax.set_zlim(0, 100)
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    fnum = 1005
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, i_grid[:,:,opt_sel[2],opt_sel[3]].T*1e3, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    ax.scatter(X, Y, get_zero_grid(), c = np.reshape(sel_i[:,:,opt_sel[2],opt_sel[3]].T, sel_i[:,:,opt_sel[2],opt_sel[3]].size), cmap= cmap_mask , edgecolor='none', alpha=0.25)
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('i [mA]')
    plt.title('Current @ {:.2f}kHz {:.2f}V  - {:s}'.format(freq_khz, max_v,coil['sensor']))
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')


    fnum = 1006
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, phi_grid[:,:,opt_sel[2],opt_sel[3]].T*1e6, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    ax.scatter(X, Y, get_zero_grid(), c = np.reshape(final_mask[:,:,opt_sel[2],opt_sel[3]].T, final_mask[:,:,opt_sel[2],opt_sel[3]].size), cmap= cmap_mask , edgecolor='none', alpha=0.25) #final result
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('phi [uWb]')
    plt.title('Flux @ {:.2f}kHz {:.2f}V  - {:s}'.format(freq_khz, max_v,coil['sensor']))
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    fnum = 1007
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, n_grid[:,:,opt_sel[2],opt_sel[3]].T, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    ax.scatter(X, Y, get_zero_grid(), c = np.reshape(final_mask[:,:,opt_sel[2],opt_sel[3]].T, final_mask[:,:,opt_sel[2],opt_sel[3]].size), edgecolor='none',cmap= cmap_mask,  alpha=0.25) #final result
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('n [1]')
    plt.title('Number of turns - {:s}'.format(coil['sensor']))
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    fnum = 1008
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, q_grid[:,:,opt_sel[2],opt_sel[3]].T, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    ax.scatter(X, Y, get_zero_grid(), c = np.reshape(final_mask[:,:,opt_sel[2],opt_sel[3]].T, final_mask[:,:,opt_sel[2],opt_sel[3]].size), edgecolor='none',cmap= cmap_mask,  alpha=0.25) #final result
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('Q [1]')
    plt.title('Quality Factor - {:s}'.format(coil['sensor']))
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    fnum = 1009
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, iratio_grid[:,:,opt_sel[2],opt_sel[3]].T, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    ax.scatter(X, Y, get_zero_grid(), c = np.reshape(final_mask[:,:,opt_sel[2],opt_sel[3]].T, final_mask[:,:,opt_sel[2],opt_sel[3]].size), edgecolor='none',cmap= cmap_mask,  alpha=0.25) #final result
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('iratio [1]')
    plt.title('iratio - {:s}'.format(coil['sensor']))
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')


    
    fnum = 1010
    plt.figure(fnum)
    plt.scatter(X, Y, c = final_mask[:,:,opt_sel[2],opt_sel[3]].T, cmap= cmap_mask, edgecolor='none', alpha=0.25)
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    # print(cu_w_list[opt_x][0]*1.0e3, cu_d_list[opt_y][0]*1.0e3, int(n_grid.T[opt_x,opt_y][0]) )
    # print(l_grid.T)
    str_plt = "L={:.2f}uH R={:.2f}ohm C={:.2f}nF Iratio={:.2f} Q={:.2f} Fres={:.2f}MHz \n i={:.2f}mA cu_w={:.2f}mm cu_d={:.2f}mm a0={:.2f}mm n={:d} l={:d}".\
    format(l_grid[opt_sel]*1e6, r_grid[opt_sel], c_grid[opt_sel]*1e9, iratio_grid[opt_sel], q_grid[opt_sel], fres_grid[opt_sel]*1e-6, i_grid[opt_sel]*1e3, cu_w_grid[opt_sel]*1.0e3, cu_d_grid[opt_sel]*1.0e3,  a0_grid[opt_sel]*1.0e3, int(n_grid[opt_sel]), int(layers_grid[opt_sel]) ) 
    plt.title('Final result {:s} @ {:.2f}kHz {:.2f}V Rg={:.2f}'.format(coil['sensor'], freq_khz, max_v, rg_grid[opt_sel]) + '\n' + str_plt, fontsize=10)
    plt.plot(cu_w_grid[opt_sel]*1.0e3, cu_d_grid[opt_sel]*1.0e3,'*r',markersize=15)
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    # str_plt = 'cu_w=' + str(cu_w_list[opt_x][0]*1e3) + ' cu_d=' + str(cu_d_list[opt_y][0]*1e3)
    # plt.text(cu_w_list[opt_x][0]*1e3, cu_d_list[opt_y][0]*1e3, str_plt, wrap=True,  bbox=dict(facecolor='white', alpha=0.25))



    fnum = 1011
    plt.figure(fnum)
    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, rg_grid[:,:,opt_sel[2],opt_sel[3]].T, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.25)
    ax.scatter(X, Y, get_zero_grid(), c = np.reshape(final_mask[:,:,opt_sel[2],opt_sel[3]].T, final_mask[:,:,opt_sel[2],opt_sel[3]].size), edgecolor='none',cmap= cmap_mask,  alpha=0.25) #final result
    plt.xlabel('cu_w [mm]')
    plt.ylabel('cu_d [mm]')
    ax.set_zlabel('Rg [ohm]')
    plt.title('Rg - {:s}'.format(coil['sensor']))
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')

    
    fnum = 1
    pcd.draw_coil(coil)
    plt.savefig(dir_path + '/figs/' + str(fnum)+ '_' + coil['sensor'] + '_{:s}'.format(note)+'.png')


    with open( dir_path + "/figs/{:s}_{:s}_out.json".format(coil['sensor'],note), 'w') as outfile:
        json.dump(coil, outfile)

    plt.show()
    



