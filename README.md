# README #

* Collection fo script for the design of pancake coils.
* There are no dependencies to third part modules at today.

### Simple usage guidelines ###

* Add your new coils design under "src/coils_geom".

  * from "template.json" to start from scratch
  * from a preexisting coil design

* Adapt the script `simple.py` in `examples` subfolder to work on your file
  and run it in an iPython console (cut+paste). See below for details

* Commit your changes (the `coils_geom` is part of the git). Not necessary
  to include the outputs.

* Some important notes:

  * Convention: the name of the sensor (in sensor) should match the JSON
    file name, e.g. "sensor": "MD8X8Y_TXRX_2LAYERS_V3" for
    MD8X8Y_TXRX_2LAYERS_V3.json
  * Use the description, sensor and _notes fields in the JSON to describe
    your design
  * The fields are described in the corresponding field prefixed with 2 underscores: __[field] is description for [field]
  * The fields are also described in the field_definitions

#### Running simple.py script ####

(adapt the paths and the environments; base2 is a standard Python 2.7
anaconda environment; you may have to change the paths in the script itself
(normally only if you run it with ipython))

With ipython:

>>> cd C:\Users\gille\Documents\codes\sensor_design\src
>>> activate base2
>>> ipython --gui=qt --matplotlib=qt

then, cut and paste parts.

or run it with python:

>>> cd C:\Users\gille\Documents\codes\sensor_design\src\examples
>>> activate base2
>>> python simple.py



### Contribution and usage guidelines ###

* add your new coils design under "src/coils_geom". 
* generate the coil descriptor using "template.json" as a reference.
* make sure to name properly the descriptor files according to the product number reference. If in doubt ask.
* always comment your code and add the relative documentation under '/doc'
* for the sensors that are used for production also, please add a reference
  in the Sensima wiki also; see the "Sensors" section in the [PCB
  page](http://adminl.sedect.ch/wiki/doku.php?id=pcb:main)

### Reference on the coil geometries and parameters ###
* image to be inserted....
<!-- ![Alt text](http://www.addictedtoibiza.com/wp-content/uploads/2012/12/example.png) -->

### Who do I talk to? ###

* gilles.santi@sensima.ch
* enrico.gasparin@sensima.ch

