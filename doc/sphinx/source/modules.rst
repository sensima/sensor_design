Modules
=======

Calculations
------------

.. automodule:: pancake_coils_calc
   :members:
   :show-inheritance:

Plotting
--------

.. automodule:: pancake_coils_draw
   :members:
   :show-inheritance:

Misc
----

.. automodule:: misc
   :members:
   :show-inheritance:

.. automodule:: passive_components
   :members:
   :show-inheritance:

.. automodule:: discretize_coil
   :members:
   :show-inheritance:


