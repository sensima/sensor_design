.. sensor_design documentation master file, created by
   sphinx-quickstart on Fri Apr  9 13:54:28 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sensor_design's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   modules
   examples


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
